
const crypto = require('crypto');
const fs = require('fs');
const msg = "channel_id=021&channel_serial_no=20181029155144179903003&partner_id=50000001&partner_serial_no=1540799502782&view=_input_charset=utf-8&body=2323&it_b_pay=2018-10-29 16:21:44&notify_url=http://120.27.166.77:6101/egs/yunpay/gateway/aliPayNotify.htm&out_trade_no=20181029155144179903003&partner=2088811089789645&payment_type=1&seller_id=2088811089789645&service=mobile.securitypay.pay&sign_type=RSA&subject=88&total_fee=0.01"
const signature = "XPq0fM3zWsyOvpR8HQMH4X4gIdVIZmQw%2BmNQOlgJd%2BG79OHlAuHOEUs1XlOnetOS%2BflMioCEUpbP3JA1CVBCEy20Yw9z0yoLJXk4tvxHcTRPuyQIWrF2I216Of10ltSCJBNcuwmZEt1LQnyOLzL6LYDAwYd1qQpaNE9WX62he2s%3D"
const hash = crypto.createHash('sha256');

hash.on('readable', () => {
  const data = hash.read();
  if (data) {
    const signShortStr = data.toString('hex').toUpperCase();
    const key = fs.readFileSync('./hf_pub.key', 'utf8');
    const verify = crypto.createVerify('RSA-SHA1');
    verify.update(signShortStr);
    console.log(verify.verify(key, signature));
  }
});

hash.write(msg);
hash.end();

