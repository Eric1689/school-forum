const QcloudSms = require("qcloudsms_js");

// 短信应用SDK AppID
const appid = 1400158921;  // SDK AppID是1400开头

// 短信应用SDK AppKey
const appkey = "4e854f058a9bdeb53b1705774b9c9531";

// 需要发送短信的手机号码
// var phoneNumbers = ["21212313123", "12345678902", "12345678903"];

// 短信模板ID，需要在短信应用中申请
const templateId = 223053;  // NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请
//templateId 7839 对应的内容是"您的验证码是: {1}"
// 签名
const smsSign = "杭州学大经财科技有限公司";  // NOTE: 这里的签名只是示例，请使用真实的已申请的签名, 签名参数使用的是`签名内容`，而不是`签名ID`

// 实例化QcloudSms
const qcloudsms = QcloudSms(appid, appkey);

function getFRandom() {
  return parseInt(Math.random()*(9999-1000+1)+1000,10) + '';
}

const ssender = qcloudsms.SmsSingleSender();
const code = getFRandom();
const params = [code];
ssender.sendWithParam(86, '15867164299', templateId, params, smsSign, "", "", function (err, res, resData) {
    if (err) {
        console.log("err: ", err);
    } else {
        console.log("request data: ", res.req);
        console.log("response data: ", resData);
    }
}); 