import React from "react";
import { Avatar } from "antd";
import moment from "moment";

const dateTimeFormat = "YYYY-MM-DD HH:mm:ss";

export const userBeans = [
  {
    title: "用户名",
    key: "name",
    tag: 'input'
  },
  {
    title: "电话号码",
    key: "tel",
    tag: 'input'
  },
  {
    title: "昵称",
    key: "nickname",
    tag: 'input'
  },
  {
    title: "头像",
    key: "avatar",
    tag: 'input',
    render: (text)=><Avatar src={text}/>
  },
];

export const postBeans = [
  {
    title: "标题",
    key: "title",
    tag: 'input',
    // editDisabled: true
  },
  {
    title: "内容",
    key: "content",
    tag: 'input',
    // editDisabled: true
  },
  {
    title: "附件图片",
    key: "imgs",
    tag: 'input',
  },
  {
    title: "所属分类",
    key: "category",
    tag: "select",
    options: ["社区话题", "信息广场", "缘分天空"],
  }
]

export const schoolBeans = [
  {
    title: "学校名称",
    key: "name",
    tag: 'input',
  },
  {
    title: "学校名称缩写",
    key: "shortName",
    tag: 'input',
  },
  {
    title: "学校logo",
    key: "logo",
    tag: 'input',
    render: (text)=><img src={text} style={{width: 30}}/>
  },
  {
    title: "论坛名称",
    key: "bssName",
    tag: 'input',
  },
  {
    title: "论坛分类",
    key: "bbsCategories",
    tag: 'input',
  },
  {
    title: "二手分类",
    key: "shCategories",
    tag: 'input',
  },
  {
    title: "学院",
    key: "colleges",
    tag: 'input',
    render: (text)=>JSON.stringify(text)
  },
  {
    title: "专业",
    key: "majors",
    tag: 'input',
  },
  {
    title: "年级",
    key: "grades",
    tag: 'input',
  },
  {
    title: "快递点",
    key: "deliveryPlaces",
    tag: 'input',
  },
]

export const courseBeans = [
  {
    title: "课程名称",
    key: "courseName",
    tag: 'input',
  },
  {
    title: "教师名字",
    key: "teacher",
    tag: 'input',
  },
  {
    title: "标签",
    key: "tags",
    tag: 'input',
    render: (text = []) => {console.log(111, text); return null}
  },
  {
    title: "评价",
    key: "msgs",
    tag: 'input',
    render: (text = []) => {console.log(222, text); return null}
  },
  {
    title: "打分",
    key: "score",
    tag: 'input',
    render: (text = []) => {console.log(text); return text.join('，')}
  },
]

export const errandBeans = [
  {
    title: "取货地址",
    key: "fromPlace",
    tag: 'input',
  },
  {
    title: "送货地址",
    key: "toPlace",
    tag: 'input',
  },
  {
    title: "开始时间",
    key: "startTime",
    tag: 'date',
    render: (text)=>moment(text*1000).format(dateTimeFormat)
  },
  {
    title: "送达时间",
    key: "endTime",
    tag: 'date',
    render: (text)=>moment(text*1000).format(dateTimeFormat)
  },
  {
    title: "联系方式",
    key: "tel",
    tag: 'input',
  },
  {
    title: "物品描述",
    key: "desc",
    tag: 'input',
  },
  {
    title: "备注",
    key: "note",
    tag: 'input',
  },
  {
    title: "上传货架号等",
    key: "imgs",
    tag: 'input',
  },
  {
    title: "有效证件",
    key: "idImgs",
    tag: 'input',
  },
  {
    title: "性别",
    key: "gender",
    tag: 'input',
  },
  {
    title: "价格",
    key: "price",
    tag: 'input',
  },
  {
    title: "类型",
    key: "type",
    tag: 'select',
    options: ['跑腿', '帮帮忙']
  }
]

export const secondhandBeans = [
  {
    title: "标题",
    key: "title",
    tag: 'input',
  },
  {
    title: "内容",
    key: "content",
    tag: 'input',
  },
  {
    title: "图片",
    key: "imgs",
    tag: 'input',
    render: (text)=><img src={text} style={{width: 30}}/>
  },
  {
    title: "分类",
    key: "category",
    tag: 'input',
  },
  {
    title: "截止时间",
    key: "endTime",
    tag: 'date',
    render: (text)=>moment(text*1000).format(dateTimeFormat)
  },
]

export const goodBeans = [
  {
    title: "商品名",
    key: "name",
    tag: 'input',
  },
  {
    title: "积分",
    key: "point",
    tag: 'input',
  },
  {
    title: "图片",
    key: "url",
    tag: 'input',
    render: (text)=><img src={text} style={{width: 30}}/>
  },
  {
    title: "说明",
    key: "desc",
    tag: 'textarea'
  },
  {
    title: "有效期范围",
    key: "range",
    tag: 'range'
  }
]

export const transferBeans = [
  {
    title: "转账方",
    key: "fromUserId",
    tag: 'input',
  },
  {
    title: "接收方",
    key: "toUserId",
    tag: 'input',
  },
  {
    title: "金额",
    key: "money",
    tag: 'input'
  },
  {
    title: "提示",
    key: "msg",
    tag: 'input',
  },
  {
    title: "状态",
    key: "isDone",
    tag: 'input',
    render: (text)=>text === 0 ?'未完成':'已处理'
  },
  {
    title: "支付方式",
    key: "payTool",
    tag: 'input',
    render: (text)=>typeof text === 'number' ?(text === 0?'支付宝':'微信'):'还未付款'
  },
]

export const reportBeans = [
  {
    title: "类型",
    key: "type",
  },
  {
    title: "对象ID",
    key: "targetId",
  },
  {
    title: "说明",
    key: "msg",
  },
  {
    title: "举报者ID",
    key: "reportId",
    tag: 'range'
  }
]


