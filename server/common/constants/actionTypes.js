// app data
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const RECEIVE_TAGS = 'RECEIVE_TAGS';
export const RECEIVE_ROLES = 'RECEIVE_ROLES';
export const RECEIVE_DOCS = 'RECEIVE_DOCS';