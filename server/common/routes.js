import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from '../common/components/App';
// import HomePage from '../common/containers/HomePage';
import SchoolManage from '../common/containers/SchoolManage/';
import ReportManage from '../common/containers/ReportManage/';
import UserManage from '../common/containers/UserManage/';
import PostManage from '../common/containers/PostManage/';
import SecondhandManage from '../common/containers/SecondhandManage/';
import CourseManage from '../common/containers/CourseManage/';
import ErrandManage from '../common/containers/ErrandManage/';
import LogIn from '../common/components/Login/';
import Analysis from '../common/containers/Analysis/';
import PostMsgManage from '../common/containers/PostMsgManage/';
import UserReport from '../common/containers/UserReport/';
import MoneyReport from '../common/containers/MoneyReport/';
import GoodManage from '../common/containers/GoodManage/';
import ServiceManage from '../common/containers/ServiceManage/';
import TransferManage from '../common/containers/TransferManage/';

const routes = (
    <Route path="/" component={App}>
        <IndexRoute component={Analysis} />
        <Route path="/schoolManage" component={SchoolManage} />
        <Route path="/reportManage" component={ReportManage} />
        <Route path="/postMsgManage" component={PostMsgManage} />
        <Route path="/userManage" component={UserManage} />
        <Route path="/postManage" component={PostManage} />
        <Route path="/secondhandManage" component={SecondhandManage} />
        <Route path="/courseManage" component={CourseManage} />
        <Route path="/errandManage" component={ErrandManage} />
        <Route path="/login" component={LogIn} />
        <Route path="/userReport" component={UserReport} />
        <Route path="/moneyReport" component={MoneyReport} />
        <Route path="/goodManage" component={GoodManage} />
        <Route path="/serviceManage" component={ServiceManage} />
        <Route path="/transferManage" component={TransferManage} />
    </Route>
);

export default routes;