import React from "react";
import { connect } from "react-redux";
import { Link, browserHistory } from "react-router";
import { fetchUser } from "../actions/actions";
import { logOut } from "../actions/actions";
import { Menu, Icon, Layout, Breadcrumb, Modal } from "antd";
import fetch from "../utils/fetch";
const { Header, Footer, Sider, Content } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const tips = [
  "请先添加标签，其他类别的管理（角色和文档）需要用到标签",
  "在进行用户管理之前，先添加角色和文档，因为添加用户的时候如果选择在学成员，需要给与角色和导读任务（导读任务需要文档）"
];

require("../../assets/styles/app.scss");
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      helpModalShown: false,
      notice: ""
    };
  }
  handleLogout = () => {
    const { dispatch } = this.props;
    localStorage.removeItem("token");
    dispatch(logOut());
    browserHistory.push("/");
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchUser());
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  logout = () => {
    const { dispatch } = this.props;
    localStorage.removeItem("token");
    dispatch(logOut());
    browserHistory.push("/login");
  };

  showHelpModal = () => {
    this.setState({ helpModalShown: true });
  };

  goToUrl = pathname => {
    browserHistory.push({
      pathname
    });
  };

  setNotice = () => {
    const { notice } = this.state;

    const newNotice = prompt('请输入公告', notice);
    if (newNotice) {
      fetch('/api/setNotice', { notice: newNotice });
      this.setState({ notice: newNotice });
    }
  }

  render() {
    const { children, user } = this.props;
    const { helpModalShown } = this.state;
    if (location.pathname === "/login") {
      return <div>{children}</div>;
    }
    // else if(!user.name) {
    //     return null;
    // }
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" style={{ height: 64 }} />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="">
                  <Link to="/">
                    <Icon type="area-chart" />
                    <span>首页</span>
                  </Link>
            </Menu.Item>
            <SubMenu key="sub1" title={<span><Icon type="setting" /><span>报表管理</span></span>}>
              <Menu.Item key="1">
                <Link to="/userReport">
                  <Icon type="area-chart" />
                  <span>用户报表</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/moneyReport">
                  <Icon type="area-chart" />
                  <span>资金报表</span>
                </Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" title={<span><Icon type="setting" /><span>内容管理</span></span>}>
              {/* <Menu.Item key="schoolManage">
                <Link to="/schoolManage">
                  <Icon type="area-chart" />
                  <span>学校管理</span>
                </Link>
              </Menu.Item> */}
              <Menu.Item key="reportManage">
                <Link to="/reportManage">
                  <Icon type="area-chart" />
                  <span>举报管理</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="postManage">
                <Link to="/postManage">
                  <Icon type="area-chart" />
                  <span>帖子/头条管理</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="userManage">
                <Link to="/userManage">
                  <Icon type="area-chart" />
                  <span>用户管理</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="postMsgManage">
                <Link to="/postMsgManage">
                  <Icon type="area-chart" />
                  <span>评论管理</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="5">
                <Link to="/goodManage">
                  <Icon type="area-chart" />
                  <span>积分商品管理</span>
                </Link>
              </Menu.Item>
              {/* <Menu.Item key="6">
                <Link to="/serviceManage">
                  <Icon type="area-chart" />
                  <span>客服管理</span>
                </Link>
              </Menu.Item> */}
              <Menu.Item key="7">
                <Link to="/transferManage">
                  <Icon type="area-chart" />
                  <span>人工转账管理</span>
                </Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: "#fff", padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
              onClick={this.toggle}
              style={{ fontSize: 20, marginLeft: 16, cursor: "pointer" }}
            />
            <span style={{ float: "right" }}>
              <a onClick={this.setNotice} style={{ marginRight: 16 }}>
                公告
              </a>
              <a onClick={this.logout} style={{ marginRight: 16 }}>
                退出
              </a>
            </span>
          </Header>
          <Content
            style={{
              margin: "16px",
              padding: 24,
              background: "#fff",
              minHeight: 500
            }}
          >
            {children}
          </Content>
          <Footer style={{ textAlign: "center" }}>Create by Lewin</Footer>
        </Layout>
        <Modal
          title="小提示"
          visible={helpModalShown}
          onOk={() => this.setState({ helpModalShown: false })}
          onCancel={() => this.setState({ helpModalShown: false })}
        >
          {tips.map((t, i) => (
            <p key={i}>
              {i + 1}：{t}
            </p>
          ))}
        </Modal>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  const { user, postsByAuthor, selectedAuthor } = state;
  const { items: posts } = postsByAuthor[selectedAuthor] || {
    items: []
  };
  return {
    user,
    posts: posts || []
  };
}
export default connect(mapStateToProps)(App);
