import React from "react";
import { Link, browserHistory } from "react-router";
import { Menu } from "antd";
import { logIn, logOut } from "../actions/actions";
import { connect } from "react-redux";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: "list",
    };
  }

  handleNavigator = e => {
    if (e.key === "logout") {
      const { dispatch } = this.props;
      localStorage.removeItem("token");
      dispatch(logOut());
        browserHistory.push("/login");
    } else {
      this.setState({
        current: e.key
      });
    }
  };

  render() {
    const { user } = this.props;
    return (
      <div>
        <Menu
          selectedKeys={[this.state.current]}
          theme="dark"
          onClick={this.handleNavigator}
          mode="horizontal"
          style={{ padding: "0 30px" }}
        >
          <Menu.Item key="logo" style={{ width: "12%" }}>
            <Link to="/">
              <img src="https://o4j806krb.qnssl.com/public/images/cnodejs_light.svg" />
            </Link>
          </Menu.Item>
          <Menu.Item key="list">
            <Link to="/">首页</Link>
          </Menu.Item>
          <Menu.Item key="userManage">
            <Link to="/userManage">用户管理</Link>
          </Menu.Item>
          <Menu.Item key="docManage">
            <Link to="/docManage">文档管理</Link>
          </Menu.Item>
          <Menu.Item key="projectManage">
            <Link to="/projectManage">项目管理</Link>
          </Menu.Item>
          <Menu.Item key="taskManage">
            <Link to="/taskManage">任务管理</Link>
          </Menu.Item>
          <Menu.Item key="notice">
            公告
          </Menu.Item>
          {user.name ? (
            [
              /* <Menu.Item key="space">
                <Link to="/space">个人中心</Link>
              </Menu.Item>,
              <Menu.Item key="publish">
                <Link to="/publish">发表文章</Link>
              </Menu.Item>, */
              <Menu.Item key="userinfo">用户:{user.name}</Menu.Item>,
              <Menu.Item key="logout">退出</Menu.Item>
            ]
          ) : (
            <Menu.Item key="login">登录</Menu.Item>
          )}
        </Menu>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user
  };
}
export default connect(mapStateToProps)(Header);
