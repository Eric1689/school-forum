import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Select, Icon } from "antd";
import DocTable from "./DocTable";
import { cloneDeep } from "lodash";
import "./index.scss";

const Option = Select.Option;

class DocManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showDocModal: false,
      selectDoc: {},
      action: "add",
      filterData: {
        docName: "",
        tags: []
      },
    };
  }
  componentDidMount() {
    this.onSearch();
  }



  getDocService = (current, pageSize, filterData) => {
    fetch("/api/getDocs", {
      current,
      pageSize,
      ...filterData
    }).then(res => {
      const { pagination } = this.state;
      pagination.total = res.total;
      pagination.current = current;
      this.setState({ list: res.docs, pagination });
    });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          docName: "",
          tags: []
        }
      },
      this.onSearch
    );
  };

  onSearch = () => {
    const { user } = this.props;
    const filterData = cloneDeep(this.state.filterData);
    const { current, pageSize } = this.state.pagination;
    const { docName } = filterData;
    if (docName) {
      filterData.docName = {
        $regex: docName,
        $options: "i"
      };
    } else {
      delete filterData.docName;
    }
    if (user.isAdmin === '否') {
      filterData.uploader = user.name;
    }
    if (filterData.tags.length !== 0) {
      filterData.tags = {
        $in: filterData.tags
      };
    } else {
      delete filterData.tags;
    }
    this.getDocService(current, pageSize, filterData);
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      filterData
    } = this.state;
    const { tags, addFromLib, fileList } = this.props;
    return (
      <div className="doc-modal-wrapper">
        <Row className="search-bar" gutter={16} type="flex" align="top">
          <Col span={6}>
            <Input
              placeholder="输入文件名"
              value={filterData.docName}
              onChange={e => this.onFilterChange(e.target.value, "docName")}
            />
          </Col>
          <Col span={12}>
            <Select
              mode="tags"
              style={{ width: "100%" }}
              placeholder="选择标签"
              onChange={value => this.onFilterChange(value, "tags")}
              value={filterData.tags}
            >
              {tags.map(t => <Option key={t._id}>{t.tagName}</Option>)}
            </Select>
          </Col>

          <Button onClick={this.onSearch}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <DocTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getDocService={this.getDocService}
          editDoc={this.editDoc}
          onRowSelect={this.onRowSelect}
          tags={tags}
          fileList={fileList}
          addDoc={addFromLib}
        />
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { user, appData } = state;
  return {
    user,
    tags: appData.tags,
  };
}
export default connect(mapStateToProps)(DocManage);
