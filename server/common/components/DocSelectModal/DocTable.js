import React from "react";
import { Button, Table, Icon } from "antd";

const DocTable = ({
  pagination,
  list,
  selectedRowKeys,
  getDocService,
  onRowSelect,
  tags = [],
  fileList,
  addDoc
}) => {
  const fileIds = fileList.filter(fl=>fl._id).map(fl=>fl._id);
  const columns = [
    {
      title: "文件名",
      dataIndex: "docName",
      key: "docName"
    },
    {
      title: "标签",
      dataIndex: "tags",
      key: "tags",
      render: (text) => tags.filter(t=>text.includes(t._id)).map(t=>t.tagName).join('，')
    },
    {
      title: "操作",
      key: "operation",
      fixed: 'right',
      width: 60,
      render: (text, record) => {
        return (
          <span>
            {fileIds.includes(record._id)?'已添加':
            <Button onClick={()=>addDoc(record)}>
              <Icon type="plus" />
            </Button>}
          </span>
        );
      }
    }
  ];
  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getDocService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      bordered
      columns={columns}
      size="small"
      rowKey={record => record._id}
    />
  );
};

export default DocTable;
