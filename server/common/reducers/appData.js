import {
  RECEIVE_USERS,
  RECEIVE_TAGS,
  RECEIVE_ROLES,
  RECEIVE_DOCS
} from "../constants/actionTypes";

function appData(state = { users: [], tags: [], roles: [], docs: [] }, action) {
  switch (action.type) {
    case RECEIVE_USERS:
      return { ...state, users: action.users };
    case RECEIVE_TAGS:
      return { ...state, tags: action.tags };
    case RECEIVE_ROLES:
      return { ...state, roles: action.roles };

    case RECEIVE_DOCS:
      return { ...state, docs: action.docs };

    default:
      return state;
  }
}

export default appData;
