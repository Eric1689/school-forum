import fetch from "../utils/fetch";
import { RECEIVE_USERS, RECEIVE_TAGS, RECEIVE_ROLES, RECEIVE_DOCS } from '../constants/actionTypes';

export function receiveUsers(users) {
    return {
        type: RECEIVE_USERS,
        users
    };
}
export function fetchUsers() {
    return (dispatch) => {
        return fetch(`/api/getAllUsers`).then(users => {
            dispatch(receiveUsers(users));
        });
    };
}

export function receiveTags(tags) {
    return {
        type: RECEIVE_TAGS,
        tags
    };
}

export function fetchTags() {
    return (dispatch) => {
        return fetch(`/api/getAllTags`).then(tags => {
            dispatch(receiveTags(tags));
        });
    };
}

export function receiveRoles(roles) {
    return {
        type: RECEIVE_ROLES,
        roles
    };
}

export function fetchRoles() {
    return (dispatch) => {
        return fetch(`/api/getAllRoles`).then(roles => {
            dispatch(receiveRoles(roles));
        });
    };
}

export function receiveDocs(docs) {
    return {
        type: RECEIVE_DOCS,
        docs
    };
}

export function fetchDocs() {
    return (dispatch) => {
        return fetch(`/api/getAllDocs`).then(docs => {
            dispatch(receiveDocs(docs));
        });
    };
}
