import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select, DatePicker } from "antd";
import { cloneDeep } from "lodash";
import ReactEcharts from 'echarts-for-react';
import "./index.scss";

const { RangePicker } = DatePicker;
const dateFormat = "YYYY-MM-DD";

class SecondhandManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRange: [],
      option: {
        title: {
          text: '每日收入'
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['每日增量', '当日总量']
        },
        toolbox: {
          feature: {
            saveAsImage: {}
          }
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
          }
        ],
        yAxis: [
          {
            type: 'value'
          }
        ],
        series: [
          {
            name: '每日增量',
            type: 'line',
            stack: '总量',
            areaStyle: { normal: {} },
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '当日总量',
            type: 'line',
            stack: '总量',
            areaStyle: { normal: {} },
            data: [220, 182, 191, 234, 290, 330, 310]
          }
        ]
      },
      option1: {
        title: {
          text: '每日支出'
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['每日增量', '当日总量']
        },
        toolbox: {
          feature: {
            saveAsImage: {}
          }
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
          }
        ],
        yAxis: [
          {
            type: 'value'
          }
        ],
        series: [
          {
            name: '每日增量',
            type: 'line',
            stack: '总量',
            areaStyle: { normal: {} },
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: '当日总量',
            type: 'line',
            stack: '总量',
            areaStyle: { normal: {} },
            data: [220, 182, 191, 234, 290, 330, 310]
          }
        ]
      }
    };
  }
  componentDidMount() {
    this.getUserReportService(1, 10);
  }

  getUserReportService = (current, pageSize) => {
    this.setState({ showChart: false })
    const [startDate, endDate] = this.state.selectedRange;
    const query = {
      type: 0
    };
    if (startDate) {
      const sDate = startDate.set({ hour: 0, minute: 0, second: 0 }).unix();
      const eDate = endDate.set({ hour: 23, minute: 59, second: 59 }).unix();
      query.createTime = { $gt: sDate, $lt: eDate }
    }
    fetch("/api/getAllStaticss", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { label, daily, total } = data.reduce((t, c) => {
        t.label.push(c.dateString);
        t.daily.push(c.dailyValue);
        t.total.push(c.sum);
        return t;
      }, { label: [], daily: [], total: [] });
      const { option } = this.state;
      option.xAxis[0].data = ['111', '2222'];
      option.series[0].data = [20, 18];
      option.series[1].data = [9, 2];
      this.setState({ option, showChart: true });
    });
  };

  render() {
    const { selectedRange, option, option1, showChart } = this.state;

    return (
      <div className="money-report-wrapper">
        <Row gutter={32}>
          <Col span={12}>
            <Row>
              请选择日期：
              <RangePicker
                style={{ width: 300 }}
                format={dateFormat}
                value={selectedRange}
                onChange={(date) => this.setState({ selectedRange: date })}
                placeholder={['开始日期', '结束日期']}
              />
            </Row>
            {
              showChart && <ReactEcharts
                option={option}
                style={{ height: '350px', width: '100%' }}
                className='react_for_echarts'
              />
            }
          </Col>
          <Col span={12}>
            <Row gutter={16}>
              请选择日期：
              <RangePicker
                style={{ width: 300 }}
                format={dateFormat}
                value={selectedRange}
                onChange={(date) => this.setState({ selectedRange: date })}
                placeholder={['开始日期', '结束日期']}
              />
            </Row>
            {
              showChart && <ReactEcharts
                option={option1}
                style={{ height: '350px', width: '100%' }}
                className='react_for_echarts'
              />
            }
          </Col>
        </Row>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { secondhand, appData } = state;
  return {
    secondhand,
    secondhands: appData.secondhands,
  };
}
export default connect(mapStateToProps)(SecondhandManage);
