import React from "react";
import { connect } from "react-redux";
import SimpleChart from "../../components/charts/";

class Analysis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <div>
        这里可以放各种统计图表
        <br />
        比如：<strong>近七日洗衣订单数，每日会员购买数</strong>
        <br />
        <br />
        <SimpleChart />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { user, appData } = state;
  return {
    user,
    tags: appData.tags,
  };
}
export default connect(mapStateToProps)(Analysis);
