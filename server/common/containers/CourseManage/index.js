import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import CourseTable from "./CourseTable";
import CourseModal from "./CourseModal";
import { cloneDeep } from "lodash";
import "./index.scss";
import { fetchCourses } from '../../actions/appData';

class CourseManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showCourseModal: false,
      selectCourse: {},
      action: "add",
      filterData: {
        name: ""
      }
    };
  }
  componentDidMount() {
    this.getCourseService(1, 10);
  }

  getCourseService = (current, pageSize, query = {}) => {
    fetch("/api/getCourses", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showCourseModal: false });
  };
  handleCancel = () => {
    this.setState({ showCourseModal: false });
  };

  removeCourse = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: courseIds } = this.state;
    fetch("/api/removeCourses", { courseIds }).then(() => {
      dispatch(fetchCourses());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (filterData.name) {
      filterData.name = {
        $regex: filterData.name,
        $options: "i"
      };
    } else {
      delete filterData.name;
    }
    this.getCourseService(1, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showCourseModal: false });
    this.onSearch();
  };

  editCourse = (course, action) => {
    this.setState({
      showCourseModal: true,
      selectCourse: course,
      action
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          name: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showCourseModal,
      selectCourse,
      action,
      filterData,
    } = this.state;

    return (
      <div className="course-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.name}
              onChange={e => this.onFilterChange(e.target.value, "name")}
              placeholder="输入手机号"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                showCourseModal: true,
                selectCourse: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeCourse}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <CourseTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getCourseService={this.getCourseService}
          editCourse={this.editCourse}
          onRowSelect={this.onRowSelect}
        />
        {showCourseModal && (
          <CourseModal
            visible={showCourseModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            course={selectCourse}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { course, appData } = state;
  return {
    course,
    courses: appData.courses,
  };
}
export default connect(mapStateToProps)(CourseManage);
