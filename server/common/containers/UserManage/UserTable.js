import React from "react";
import { Button, Table, Icon, Dropdown, Menu, Modal } from "antd";
import { userBeans } from "../../constants/";
import moment from "moment";

const UserTable = ({
  pagination,
  list,
  selectedRowKeys,
  getUserService,
  onRowSelect,
  authUser,
  freezUser
}) => {
  const columns = userBeans.map(({ title, key, render }) => ({
    title,
    key,
    dataIndex: key,
    render
  }));
  columns.push({
    title: '累加积分',
    key: 'point',
    dataIndex: 'point'
  })
  columns.push({
    title: '可使用积分',
    key: 'leftPoint',
    dataIndex: 'leftPoint'
  })
  columns.push({
    title: '证件图片',
    key: 'idImgs',
    dataIndex: 'idImgs',
    render: (text = [])=><a onClick={()=>Modal.info({
      content: <div>{text.map((t)=><img src={t} alt="" />)}</div>
    })}>查看</a>
  })
  columns.push({
    title: '审核状态',
    key: 'isAuth',
    dataIndex: 'isAuth',
    render: (text)=>text?'通过':'未通过'
  })
  columns.push({
    title: '冻结到期日',
    key: 'frozenTo',
    dataIndex: 'frozenTo',
    render: (text)=>text?moment(text*1000).format('YYYY-MM-DD'):'正常用户'
  })
  columns.push({
    title: "操作",
    key: "operation",
    fixed: "right",
    width: 120,
    render: (text, record) => {
      const menus = (
        <Menu>
          <Menu.Item>
            <a onClick={()=>authUser(record._id, true)}>审核通过</a>
          </Menu.Item>
          <Menu.Item>
            <a onClick={()=>authUser(record._id, false)}>审核不通过</a>
          </Menu.Item>
          <Menu.Item>
            <a onClick={()=>freezUser(record._id, true)}>冻结用户</a>
          </Menu.Item>
          <Menu.Item>
            <a onClick={()=>freezUser(record._id, false)}>解冻</a>
          </Menu.Item>
        </Menu>
      );
      return (
        <Dropdown overlay={menus} placement="bottomLeft">
          <Button>
            <Icon type="bars" />更多操作
          </Button>
        </Dropdown>
      );
    }
  });
  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getUserService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      bordered
      size="small"
      scroll={{ x: 1000 }}
      columns={columns}
      rowKey={record => record._id}
    />
  );
};

export default UserTable;
