import React from "react";
import { Button, Table, Icon, Dropdown, Menu } from "antd";
import { reportBeans } from "../../constants/";

const ReportTable = ({
  pagination,
  list,
  selectedRowKeys,
  getReportService,
  onRowSelect,
  editReport
}) => {
  const columns = reportBeans.map(({ title, key, render }) => ({
    title,
    key,
    dataIndex: key,
    render
  }));
  columns.push({
    title: "操作",
    key: "operation",
    fixed: "right",
    width: 120,
    render: (text, record) => {
      const menus = (
        <Menu>
          <Menu.Item>
            <a onClick={editReport}>编辑用户信息</a>
          </Menu.Item>
        </Menu>
      );
      return (
        <Dropdown overlay={menus} placement="bottomLeft">
          <Button>
            <Icon type="bars" />更多操作
          </Button>
        </Dropdown>
      );
    }
  });
  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getReportService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      bordered
      size="small"
      scroll={{ x: 1000 }}
      columns={columns}
      rowKey={record => record._id}
    />
  );
};

export default ReportTable;
