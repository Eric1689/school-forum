import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import ReportTable from "./ReportTable";
import { cloneDeep } from "lodash";
import "./index.scss";
import { fetchReports } from '../../actions/appData';

class ReportManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 10,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      selectReport: {},
      action: "add",
      filterData: {
        type: ""
      }
    };
  }
  componentDidMount() {
    const { pagination } = this.state;
    const { current, pageSize } = pagination;
    this.getReportService(current, pageSize);
  }

  getReportService = (current, pageSize, query = {}) => {
    fetch("/api/getReports", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  removeReport = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: reportIds } = this.state;
    fetch("/api/removeReports", { reportIds }).then(() => {
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (!filterData.type) {
      delete filterData.type;
    }
    this.getReportService(1, 10, filterData);
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          type: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      selectReport,
      action,
      filterData,
    } = this.state;

    return (
      <div className="report-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.type}
              onChange={e => this.onFilterChange(e.target.value, "type")}
              placeholder="输入类别"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                selectReport: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeReport}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <ReportTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getReportService={this.getReportService}
          editReport={this.editReport}
          onRowSelect={this.onRowSelect}
        />
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { report, appData } = state;
  return {
    report,
    reports: appData.reports,
  };
}
export default connect(mapStateToProps)(ReportManage);
