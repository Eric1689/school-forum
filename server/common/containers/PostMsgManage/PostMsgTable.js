import React from "react";
import { Button, Table, Icon, Menu, Dropdown } from "antd";
// import { postMsgBeans } from "../../constants/";


const PostMsgTable = ({
  pagination,
  list,
  selectedRowKeys,
  getPostMsgService,
  onRowSelect,
  noPass,
  remove
}) => {
  const columns = [
    {
      title: "评价内容",
      dataIndex: "content",
    },
    {
      title: "对应帖子ID",
      dataIndex: "postId",
    },
    {
      title: "评论者ID",
      dataIndex: "creatorId",
    },
    {
      title: "操作",
      key: "operation",
      fixed: "right",
      width: 120,
      render: (text, record) => {

        const menus = (
          <Menu>
            <Menu.Item>
              <a onClick={() => noPass(record)}>审核不通过</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => remove([record._id])}>删除</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menus} placement="bottomLeft">
            <Button>
              <Icon type="bars" />更多操作
            </Button>
          </Dropdown>
        );
      }
    }
  ]


  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getPostMsgService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      bordered
      size="small"
      columns={columns}
      rowKey="_id"
    />
  );
};

export default PostMsgTable;
