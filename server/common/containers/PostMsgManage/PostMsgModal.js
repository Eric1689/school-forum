import React from "react";
import { Input, Form, Modal, Select, DatePicker, Row, Col } from "antd";
import { cloneDeep, isEmpty } from "lodash";
import fetch from "../../utils/fetch";
import moment from "moment";
import { postMsgBeans } from "../../constants/";

const Option = Select.Option;
const FormItem = Form.Item;

class PostMsgModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    const postMsg = this.props.postMsg;
    if (!isEmpty(this.props.postMsg)) {
      this.props.form.setFieldsValue(postMsg);
    }
  }

  handleSave = () => {
    const { form, user, onCancel, onSearch, users } = this.props;
    form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      values = cloneDeep(values);
      const postMsg = this.props.postMsg || {};
      if (!postMsg._id) {
        // const { tel, name, address } = user;
        const creatorId = values.creatorId;
        const creator = users.filter(u => u._id === creatorId)[0] || {};
        values = { ...values, imgs: [values.imgs], creator, createTime: moment().unix() };
      }
      fetch("/api/updatePostMsg", {
        ...postMsg,
        ...values
      }).then(() => {
        onCancel();
        onSearch();
      });
    });
  };

  tagGenerator = ({ title, key, tag, options, editDisabled, required = false }) => {
    const { form, user, action } = this.props;
    const { getFieldDecorator } = form;
    const initialValue =
      tag !== "date"
        ? (user[key])
        : (user[key] ? moment(user[key], dateFormat) : null);
    let comp = null;
    if (tag === "input") {
      comp = <Input disabled={editDisabled && action === 'edit'} placeholder={"请输入" + title} />;
    } else if (tag === "select") {
      comp = (
        <Select placeholder={"请选择" + title}>
          {options.map(o => (
            <Option key={o} value={o}>
              {o}
            </Option>
          ))}
        </Select>
      );
    } else if (tag === "date") {
      comp = (
        <DatePicker
          style={{ width: "100%" }}
          format={dateFormat}
          placeholder={"请输入" + title}
        />
      );
    }

    return (
      <Col span={12} key={key}>
        <FormItem label={title}>
          {getFieldDecorator(key, {
            rules: [
              {
                required,
                message: title + '必填'
              }
            ],
            initialValue
          })(comp)}
        </FormItem>
      </Col>
    );
  };

  render() {
    const { form, visible, onCancel, action, users } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        title={(action === "add" ? "新增" : "编辑") + "文档"}
        visible={visible}
        onOk={this.handleSave}
        onCancel={onCancel}
      >
        <form>
          <Row gutter={16} type="flex" align="top">
            {postMsgBeans.map(ub => {
              return this.tagGenerator(ub);
            })}
            <Col span={12}>
              <FormItem label={'选择用户'}>
                {getFieldDecorator('creatorId')(<Select placeholder={"请选择用户"}>
                  {users.map(o => (
                    <Option key={o._id} value={o._id}>
                      {o.nickname}
                    </Option>
                  ))}
                </Select>)}
              </FormItem>
            </Col>
          </Row>
        </form>
      </Modal>
    );
  }
}

export default Form.create()(PostMsgModal);
