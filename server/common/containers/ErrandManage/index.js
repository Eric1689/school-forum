import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import ErrandTable from "./ErrandTable";
import ErrandModal from "./ErrandModal";
import { cloneDeep } from "lodash";
import "./index.scss";
import { fetchErrands } from '../../actions/appData';

class ErrandManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showErrandModal: false,
      selectErrand: {},
      action: "add",
      filterData: {
        name: ""
      }
    };
  }
  componentDidMount() {
    this.getErrandService(1, 10);
  }

  getErrandService = (current, pageSize, query = {}) => {
    fetch("/api/getErrands", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showErrandModal: false });
  };
  handleCancel = () => {
    this.setState({ showErrandModal: false });
  };

  removeErrand = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: errandIds } = this.state;
    fetch("/api/removeErrands", { errandIds }).then(() => {
      dispatch(fetchErrands());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (filterData.name) {
      filterData.name = {
        $regex: filterData.name,
        $options: "i"
      };
    } else {
      delete filterData.name;
    }
    this.getErrandService(1, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showErrandModal: false });
    this.onSearch();
  };

  editErrand = (errand, action) => {
    this.setState({
      showErrandModal: true,
      selectErrand: errand,
      action
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          name: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showErrandModal,
      selectErrand,
      action,
      filterData,
    } = this.state;

    return (
      <div className="errand-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.name}
              onChange={e => this.onFilterChange(e.target.value, "name")}
              placeholder="输入手机号"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                showErrandModal: true,
                selectErrand: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeErrand}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <ErrandTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getErrandService={this.getErrandService}
          editErrand={this.editErrand}
          onRowSelect={this.onRowSelect}
        />
        {showErrandModal && (
          <ErrandModal
            visible={showErrandModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            errand={selectErrand}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { errand, appData } = state;
  return {
    errand,
    errands: appData.errands,
  };
}
export default connect(mapStateToProps)(ErrandManage);
