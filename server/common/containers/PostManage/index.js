import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Select, Icon, Modal, Form } from "antd";
import PostTable from "./PostTable";
import PostModal from "./PostModal";
import { cloneDeep } from "lodash";
import "./index.scss";
import { fetchPosts } from '../../actions/appData';

const Option = Select.Option;
const FormItem = Form.Item;
const typeList = [
  {value: 0, label: '帖子'},
  {value: 1, label: '活动'},
  {value: 2, label: '投票'},
  {value: 3, label: '课程'},
  {value: 4, label: '二手'},
]

class PostManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 10,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showPostModal: false,
      selectPost: {},
      action: "add",
      filterData: {
        _id: "",
        creatorId: "",
        category: "",
        type: "",
      },
      modalData: {},
      showLikeModal: false,
      showCmtModal: false,
    };
  }

  componentDidMount() {
    const { pagination } = this.state;
    const { current, pageSize } = pagination;
    this.getPostService(current, pageSize);
  }

  componentWillReceiveProps(nextProps) {
    const newUser = nextProps.user;
    const user = this.props.user;
    if (!user.name && newUser.name) {
      this.onSearch(nextProps);
    }
  }

  getPostService = (current, pageSize, filterData) => {
    fetch("/api/getPosts", {
      current,
      pageSize,
      type: 'all',
      ...filterData
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleCancel = () => {
    this.setState({ showPostModal: false });
    // this.onSearch();
  };

  removePosts = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: postIds } = this.state;
    fetch("/api/removePosts", { postIds }).then(() => {
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  editPost = post => {
    this.setState({
      showPostModal: true,
      selectPost: post,
      action: "edit"
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          _id: "",
          creatorId: "",
          category: "",
          type: "",
        }
      },
      this.onSearch
    );
  };

  onSearch = (props) => {
    const { user } = props || this.props;
    const filterData = cloneDeep(this.state.filterData);
    const { current, pageSize } = this.state.pagination;
    const {_id, creatorId, category, type} = filterData;
    if (!_id) {
      delete filterData._id;
    }
    if (!creatorId) {
      delete filterData.creatorId;
    }
    if (!category) {
      delete filterData.category;
    }
    if (!type) {
      delete filterData.type;
    } else {
      filterData.type = Number(filterData.type);
    }
    this.getPostService(current, pageSize, filterData);
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  setTopPost = (post, isTop) => {
    fetch("/api/setTopPost", {_id: post._id, isTop}).then(() => {
      this.onSearch();
    });
  };
  
  noPassPost = (post) => {
    fetch("/api/noPassPost", {_id: post._id}).then(() => {
      this.onSearch();
    });
  };

  removePost = (postIds) => {
    fetch("/api/removePosts", {postIds}).then(() => {
      this.onSearch();
    });
  };

  onModalChange = (key, value) => {
    const { modalData } = this.state;
    modalData[key] = value;
    this.setState({ modalData })
  }

  handleLike = () => {
    const { modalData, selectPost } = this.state;
    const { users } = this.props;
    const params = { ...modalData };
    params.postId = selectPost._id;
    params.user = users.filter(u => u._id === modalData.user)[0];
    fetch("/api/likePost", params).then(() => {
      this.onSearch();
      this.setState({ showLikeModal: false });
    });
  };

  handleCmt = () => {
    const { modalData, selectPost } = this.state;
    const { users } = this.props;
    const params = { ...modalData };
    params.postId = selectPost._id;
    params.user = users.filter(u => u._id === modalData.user)[0];
    fetch("/api/cmtPost", params).then(() => {
      this.onSearch();
      this.setState({ showCmtModal: false });
    });
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showPostModal,
      selectPost,
      filterData,
      action,
      modalData,
      showLikeModal,
      showCmtModal,
    } = this.state;

    return (
      <div className="post-manage-wrapper">
        <Row className="search-bar" gutter={16}>
          <Col span={4}>
            <Input
              placeholder="输入ID"
              value={filterData._id}
              onChange={e => this.onFilterChange(e.target.value, "_id")}
            />
          </Col>
          <Col span={4}>
            <Select style={{ width: '100%' }} value={filterData.type} onChange={(value) => this.onFilterChange(value, 'type')} placeholder={"请选择类型"}>
                {typeList.map(o => (
                  <Option value={o.value}>
                    {o.label}
                  </Option>
                ))}
              </Select>
          </Col>
          <Col span={4}>
            <Input
              placeholder="输入分类"
              value={filterData.category}
              onChange={e => this.onFilterChange(e.target.value, "category")}
            />
          </Col>
          <Col span={4}>
            <Input
              placeholder="输入创建者ID"
              value={filterData.creatorId}
              onChange={e => this.onFilterChange(e.target.value, "creatorId")}
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            size="small"
            onClick={() =>
              this.setState({
                showPostModal: true,
                selectPost: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              size="small"
              style={{ marginLeft: 8 }}
              onClick={this.removePosts}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <PostTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getPostService={this.getPostService}
          editPost={this.editPost}
          onRowSelect={this.onRowSelect}
          setTop={this.setTopPost}
          noPass={this.noPassPost}
          remove={this.removePost}
        />
        {showPostModal && (
          <PostModal
            visible={showPostModal}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            post={selectPost}
            user={this.props.user}
            users={this.props.users}
            action={action}
            dispatch={this.props.dispatch}
          />
        )}
        <Modal
          title="点赞"
          visible={showLikeModal}
          onOk={this.handleLike}
          onCancel={() => this.setState({ showLikeModal: false })}
        >
          <FormItem label={'选择用户'}>
            <Select style={{ width: '100%' }} value={modalData.user} onChange={(value) => this.onModalChange('user', value)} placeholder={"请选择用户"}>
              {this.props.users.map(o => (
                <Option key={o._id} value={o._id}>
                  {o.nickname}
                </Option>
              ))}
            </Select>
          </FormItem>
        </Modal>

        <Modal
          title="评论"
          visible={showCmtModal}
          onOk={this.handleCmt}
          onCancel={() => this.setState({ showCmtModal: false })}
        >
          <FormItem label={'选择用户'}>
            <Select style={{ width: '100%' }} value={modalData.user} onChange={(value) => this.onModalChange('user', value)} placeholder={"请选择用户"}>
              {this.props.users.map(o => (
                <Option key={o._id} value={o._id}>
                  {o.nickname}
                </Option>
              ))}
            </Select>
          </FormItem>
          <FormItem label={'评论内容'}>
            <Input value={modalData.cmt} onChange={(e) => this.onModalChange('cmt', e.target.value)} placeholder={"请选择用户"} />
          </FormItem>
        </Modal>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { user, appData } = state;
  return {
    user,
    users: appData.users,
  };
}
export default connect(mapStateToProps)(PostManage);
