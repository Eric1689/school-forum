import React from "react";
import { Button, Table, Icon, Menu, Dropdown } from "antd";
import { postBeans } from "../../constants/";


const PostTable = ({
  pagination,
  list,
  selectedRowKeys,
  getPostService,
  onRowSelect,
  setTop,
  noPass,
  remove
}) => {
  const columns = [
    {
      title: "题目",
      dataIndex: "title",
    },
    {
      title: "内容",
      dataIndex: "content",
    },
    {
      title: "附件图片",
      dataIndex: "imgs",
      render: (text = '', record) => <span>{text.map(i=><img src={i.url} style={{width: 30}}/>)}</span>
    },
    {
      title: "一级分类",
      dataIndex: "category",
    },
    {
      title: "二级分类",
      dataIndex: "secCategory",
    },
    {
      title: "点赞数",
      dataIndex: "likes",
      render: (text = {}, record) => Object.keys(text).length
    },
    {
      title: "评论数",
      dataIndex: "cmts",
      render: (text = [], record) => text.length
    },
    {
      title: "是否是头条",
      dataIndex: "isTop",
      render: (text) => text?'是':'否'
    },
    {
      title: "操作",
      key: "operation",
      fixed: "right",
      width: 120,
      render: (text, record) => {

        const menus = (
          <Menu>
            {record.isTop ? <Menu.Item>
              <a onClick={() => setTop(record, false)}>取消头条</a>
            </Menu.Item>:<Menu.Item>
              <a onClick={() => setTop(record, true)}>设为头条</a>
            </Menu.Item>}
            <Menu.Item>
              <a onClick={() => noPass(record)}>审核不通过</a>
            </Menu.Item>
            <Menu.Item>
              <a onClick={() => remove([record._id])}>删除</a>
            </Menu.Item>
          </Menu>
        );
        return (
          <Dropdown overlay={menus} placement="bottomLeft">
            <Button>
              <Icon type="bars" />更多操作
            </Button>
          </Dropdown>
        );
      }
    }
  ]


  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getPostService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      scroll={{ x: 1000 }}
      bordered
      size="small"
      columns={columns}
      rowKey={record => record._id}
    />
  );
};

export default PostTable;
