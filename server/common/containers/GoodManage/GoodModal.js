import React from "react";
import { connect } from "react-redux";
import { Input, Form, Modal, Select, DatePicker, Row, Col } from "antd";
import { cloneDeep, intersectionBy } from "lodash";
import fetch from "../../utils/fetch";
import crypto from "crypto";
import { goodBeans } from "../../constants/";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";

// const dateFormat = "YYYY-MM-DD";
const dateTimeFormat = "YYYY-MM-DD HH:mm:ss";

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;

class GoodModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSave = () => {
    const { form, action, good } = this.props;
    form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      if (values.range[0]) {
        values.range = `${values.range[0].format('YYYY-MM-DD')}至${values.range[1].format('YYYY-MM-DD')}`
      }
      values = cloneDeep({ ...good, ...values });
      fetch("/api/updateGood", values).then(() => {
        this.props.onCancel();
        this.props.onSearch();
      });
    });
  };

  tagGenerator = ({ title, key, tag, options, required = false }) => {
    const { form, good } = this.props;
    const { getFieldDecorator } = form;
    const initialValue =
      tag !== "date"
        ? (good[key])
        : (good[key] ? moment(good[key], dateTimeFormat) : null);
    let comp = null;
    if (tag === "input") {
      comp = <Input placeholder={"请输入" + title} />;
    } else if (tag === "textarea") {
      comp = (
        <TextArea placeholder={"请输入" + title} rows={3}/>
      );
    } else if (tag === "select") {
      comp = (
        <Select placeholder={"请选择" + title}>
          {options.map(o => (
            <Option key={o} value={o}>
              {o}
            </Option>
          ))}
        </Select>
      );
    } else if (tag === "date") {
      comp = (
        <DatePicker
          showTime
          style={{ width: "100%" }}
          format={dateTimeFormat}
          placeholder={"请输入" + title}
        />
      );
    } else if (tag === 'range') {
      comp = (
        <RangePicker
          style={{ width: "100%" }}
          format={dateTimeFormat}
          placeholder={"请输入" + title}
        />
      );
    }

    return (
      <Col span={8} key={key}>
        <FormItem label={title}>
          {getFieldDecorator(key, {
            rules: [
              {
                required,
                message: title + '必填'
              }
            ],
            initialValue
          })(comp)}
        </FormItem>
      </Col>
    );
  };

  render() {
    const { form, visible, onCancel, action } = this.props;
    return (
      <Modal
        title={(action === "add" ? "新建" : "修改") + "帮助"}
        visible={visible}
        onOk={this.handleSave}
        onCancel={onCancel}
      >
        <form>
          <Row gutter={16} type="flex" align="top">
            {goodBeans.map(ub => {
              return this.tagGenerator(ub);
            })}
          </Row>
        </form>
      </Modal>
    );
  }
}

export default connect()(Form.create()(GoodModal));
