import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import GoodTable from "./GoodTable";
import GoodModal from "./GoodModal";
import { cloneDeep } from "lodash";
import "./index.scss";

class GoodManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 10,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showGoodModal: false,
      selectGood: {},
      action: "add",
      filterData: {
        name: ""
      }
    };
  }
  componentDidMount() {
    const { pagination } = this.state;
    const { current, pageSize } = pagination;
    this.getGoodService(current, pageSize);
  }

  getGoodService = (current, pageSize, query = {}) => {
    fetch("/api/getGoods", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showGoodModal: false });
  };
  handleCancel = () => {
    this.setState({ showGoodModal: false });
  };

  removeGood = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: goodIds } = this.state;
    fetch("/api/removeGoods", { goodIds }).then(() => {
      dispatch(fetchGoods());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (filterData.name) {
      filterData.name = {
        $regex: filterData.name,
        $options: "i"
      };
    } else {
      delete filterData.name;
    }
    this.getGoodService(1, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showGoodModal: false });
    this.onSearch();
  };

  editGood = (good, action) => {
    this.setState({
      showGoodModal: true,
      selectGood: good,
      action
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          name: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showGoodModal,
      selectGood,
      action,
      filterData,
    } = this.state;

    return (
      <div className="good-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.name}
              onChange={e => this.onFilterChange(e.target.value, "name")}
              placeholder="输入手机号"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                showGoodModal: true,
                selectGood: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeGood}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <GoodTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getGoodService={this.getGoodService}
          editGood={this.editGood}
          onRowSelect={this.onRowSelect}
        />
        {showGoodModal && (
          <GoodModal
            visible={showGoodModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            good={selectGood}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { good, appData } = state;
  return {
    good,
    goods: appData.goods,
  };
}
export default connect(mapStateToProps)(GoodManage);
