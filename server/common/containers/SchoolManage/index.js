import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import SchoolTable from "./SchoolTable";
import SchoolModal from "./SchoolModal";
import { cloneDeep } from "lodash";
import "./index.scss";
import { fetchSchools } from '../../actions/appData';

class SchoolManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showSchoolModal: false,
      selectSchool: {},
      action: "add",
      filterData: {
        name: ""
      }
    };
  }
  componentDidMount() {
    this.getSchoolService(1, 10);
  }

  getSchoolService = (current, pageSize, query = {}) => {
    fetch("/api/getSchools", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showSchoolModal: false });
  };
  handleCancel = () => {
    this.setState({ showSchoolModal: false });
  };

  removeSchool = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: schoolIds } = this.state;
    fetch("/api/removeSchools", { schoolIds }).then(() => {
      dispatch(fetchSchools());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (filterData.name) {
      filterData.name = {
        $regex: filterData.name,
        $options: "i"
      };
    } else {
      delete filterData.name;
    }
    this.getSchoolService(1, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showSchoolModal: false });
    this.onSearch();
  };

  editSchool = (school, action) => {
    this.setState({
      showSchoolModal: true,
      selectSchool: school,
      action
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          name: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showSchoolModal,
      selectSchool,
      action,
      filterData,
    } = this.state;

    return (
      <div className="school-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.name}
              onChange={e => this.onFilterChange(e.target.value, "name")}
              placeholder="输入手机号"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                showSchoolModal: true,
                selectSchool: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeSchool}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <SchoolTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getSchoolService={this.getSchoolService}
          editSchool={this.editSchool}
          onRowSelect={this.onRowSelect}
        />
        {showSchoolModal && (
          <SchoolModal
            visible={showSchoolModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            school={selectSchool}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { school, appData } = state;
  return {
    school,
    schools: appData.schools,
  };
}
export default connect(mapStateToProps)(SchoolManage);
