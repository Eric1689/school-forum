import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select, Modal } from "antd";
import TransferTable from "./TransferTable";
import TransferModal from "./TransferModal";
import { cloneDeep } from "lodash";
import "./index.scss";

const Option = Select.Option;

class TransferManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showTransferModal: false,
      selectTransfer: {},
      filterData: {}
    };
  }
  componentDidMount() {
    this.onSearch()
  }

  getTransferService = (current, pageSize, query = {}) => {
    fetch("/api/getTransfers", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showTransferModal: false });
  };
  handleCancel = () => {
    this.setState({ showTransferModal: false });
  };

  removeTransfer = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: transferIds } = this.state;
    fetch("/api/removeTransfers", { transferIds }).then(() => {
      dispatch(fetchTransfers());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = (page = 1) => {
    const filterData = cloneDeep(this.state.filterData);
    const { isDone, fromUserId } = filterData;
    if (fromUserId === 'all') {
      delete filterData.fromUserId
    }
    if (isDone === 'all' || !isDone) {
      delete filterData.isDone
    } else {
      filterData.isDone = isDone === 'true'
    }

    this.getTransferService(page, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showTransferModal: false });
    this.onSearch();
  };

  doTransfer = (transfer) => {
    Modal.confirm({
      title: '确定已经打完款了?',
      onOk: () => {
        fetch("/api/doTransfer", transfer).then(() => {
          this.onSearch();
        });
      },
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showTransferModal,
      selectTransfer,
      action,
      filterData,
    } = this.state;

    return (
      <div className="transfer-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Select
              style={{ width: "100%" }}
              placeholder="选择类型"
              onChange={value => this.onFilterChange(value, "fromUserId")}
              value={filterData.fromUserId}
            >
              <Option key='all'>全部</Option>
              <Option key='system'>提现</Option>
            </Select>
          </Col>
          <Col span={6}>
            <Select
              style={{ width: "100%" }}
              placeholder="选择类型"
              onChange={value => this.onFilterChange(value, "isDone")}
              value={filterData.isDone}
            >
              <Option key='all'>全部</Option>
              <Option key='false'>未操作</Option>
              <Option key='true'>已完成</Option>
            </Select>
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
        </Row>
        <Row className="tool-bar">
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeTransfer}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <TransferTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getTransferService={this.getTransferService}
          doTransfer={this.doTransfer}
          onRowSelect={this.onRowSelect}
        />
        {showTransferModal && (
          <TransferModal
            visible={showTransferModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            transfer={selectTransfer}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { transfer, appData } = state;
  return {
    transfer,
    transfers: appData.transfers,
  };
}
export default connect(mapStateToProps)(TransferManage);
