import React from "react";
import { Button, Table, Icon, Dropdown, Menu } from "antd";
import { transferBeans } from "../../constants/";

const TransferTable = ({
  pagination,
  list,
  selectedRowKeys,
  getTransferService,
  onRowSelect,
  doTransfer
}) => {
  const columns = transferBeans.map(({ title, key, render}) => ({
    title,
    key,
    dataIndex: key,
    render
  }));
  columns.push({
    title: "操作",
    key: "operation",
    fixed: "right",
    width: 160,
    render: (text, record) => record.fromUserId === 'system' && !record.isDone && <span><a style={{marginRight: 10}} onClick={()=>doTransfer({_id: record._id, payTool: 1})}>微信打款</a><a onClick={()=>doTransfer({_id: record._id, payTool: 0})}>支付宝打款</a></span>
  });
  const tableProps = {
    pagination,
    onChange: pagination => {
      const { current, pageSize } = pagination;
      getTransferService(current, pageSize);
    },
    // onDeleteItem(id) {},
    // onEditItem(item) {},
    rowSelection: {
      selectedRowKeys,
      onChange: keys => {
        onRowSelect(keys);
      }
    }
  };

  return (
    <Table
      {...tableProps}
      dataSource={list}
      bordered
      size="small"
      scroll={{ x: 1000 }}
      columns={columns}
      rowKey={record => record._id}
    />
  );
};

export default TransferTable;
