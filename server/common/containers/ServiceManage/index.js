import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch";
import { Input, Button, Row, Col, Icon, Select } from "antd";
import SecondhandTable from "./SecondhandTable";
import SecondhandModal from "./SecondhandModal";
import { cloneDeep } from "lodash";
import "./index.scss";

class SecondhandManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pagination: {
        current: 1,
        pageSize: 100,
        total: 0,
        showQuickJumper: true
      },
      selectedRowKeys: [],
      showSecondhandModal: false,
      selectSecondhand: {},
      action: "add",
      filterData: {
        name: ""
      }
    };
  }
  componentDidMount() {
    this.getSecondhandService(1, 10);
  }

  getSecondhandService = (current, pageSize, query = {}) => {
    fetch("/api/getSecondhands", {
      current,
      pageSize,
      ...query
    }).then(data => {
      const { pagination } = this.state;
      pagination.total = data.total;
      pagination.current = current;
      this.setState({ list: data.list, pagination });
    });
  };

  handleOk = () => {
    this.setState({ showSecondhandModal: false });
  };
  handleCancel = () => {
    this.setState({ showSecondhandModal: false });
  };

  removeSecondhand = () => {
    const { dispatch } = this.props;
    const { selectedRowKeys: secondhandIds } = this.state;
    fetch("/api/removeSecondhands", { secondhandIds }).then(() => {
      dispatch(fetchSecondhands());
      this.onSearch();
      this.setState({ selectedRowKeys: [] });
    });
  };

  onFilterChange = (value, key) => {
    const { filterData } = this.state;
    filterData[key] = value;
    this.setState({ filterData });
  };

  onSearch = () => {
    const filterData = cloneDeep(this.state.filterData);
    if (filterData.name) {
      filterData.name = {
        $regex: filterData.name,
        $options: "i"
      };
    } else {
      delete filterData.name;
    }
    this.getSecondhandService(1, 10, filterData);
  };

  handleCancel = () => {
    this.setState({ showSecondhandModal: false });
    this.onSearch();
  };

  editSecondhand = (secondhand, action) => {
    this.setState({
      showSecondhandModal: true,
      selectSecondhand: secondhand,
      action
    });
  };

  onRowSelect = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  resetFilter = () => {
    this.setState(
      {
        filterData: {
          name: "",
        }
      },
      this.onSearch
    );
  };

  render() {
    const {
      list,
      pagination,
      selectedRowKeys,
      showSecondhandModal,
      selectSecondhand,
      action,
      filterData,
    } = this.state;

    return (
      <div className="secondhand-manage-wrapper">
        <Row gutter={16} className="search-bar">
          <Col span={6}>
            <Input
              value={filterData.name}
              onChange={e => this.onFilterChange(e.target.value, "name")}
              placeholder="输入手机号"
            />
          </Col>
          <Button onClick={() => this.onSearch()}>
            <Icon type="search" />
          </Button>
          <Button onClick={this.resetFilter}>
            <Icon type="close" />
          </Button>
        </Row>
        <Row className="tool-bar">
          <Button
            onClick={() =>
              this.setState({
                showSecondhandModal: true,
                selectSecondhand: {},
                action: "add"
              })}
          >
            <Icon type="plus" />
          </Button>
          {selectedRowKeys.length > 0 && (
            <Button
              type="primary"
              style={{ marginLeft: 8 }}
              onClick={this.removeSecondhand}
            >
              <Icon type="close" />
            </Button>
          )}
        </Row>
        <SecondhandTable
          list={list}
          pagination={pagination}
          selectedRowKeys={selectedRowKeys}
          getSecondhandService={this.getSecondhandService}
          editSecondhand={this.editSecondhand}
          onRowSelect={this.onRowSelect}
        />
        {showSecondhandModal && (
          <SecondhandModal
            visible={showSecondhandModal}
            onCancel={this.handleCancel}
            onSearch={this.onSearch}
            secondhand={selectSecondhand}
            action={action}
          />
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { secondhand, appData } = state;
  return {
    secondhand,
    secondhands: appData.secondhands,
  };
}
export default connect(mapStateToProps)(SecondhandManage);
