import Express from "express";
import { getSchools, getSchool, getAllSchools, updateSchool, removeSchools } from "./service/schools/";
import { user, reg, adminlog, log, getValidateCode, updateUserInfo, watchUser, getUsers, viewUserInfo, updateUser, removeUsers, getAllUsers, getCaptcha, resetPasswd, dailySignin, getLoginValidateCode, validateCaptcha, chargeUser, getServiceUser } from "./service/users/";
import { getDocs, updateDoc, removeDocs, getAllDocs, uploadDocs } from "./service/docs/";
import { getPosts, getActivities, joinActivity, getPost, createPost, updatePost, removePosts, likePost, cmtPost, setTopPost, noPassPost, sharePost, unWatchPost, getMorePostMsgs, getAllPostLikes, getMyLikePosts, getMyWatchPost, addCourse, cmtCourse, searchPosts, viewPost, likePostMsg, removePostMsgs, getPostMsgs, getBest10List, removePost } from "./service/posts/";
// import { getCourses, getCourse, addCourse, updateCourse, removeCourses } from "./service/courses/";
import { getUser2Users, getUser2User, updateUser2User, removeUser2Users, getMyWatch, getMyFan } from "./service/user2Users/";
import { getSecondhands, getMySecondhands, getMyObtainSecondhands, getSecondhand, createSecondhand, obtainSecondhand, removeSecondhands, likeSecondhand, cmtSecondhand, shareSecondhand, getMoreSecondhandMsgs, searchSecondhands, viewSecondhand } from "./service/secondhands/";
import { getErrands, getErrand, createPaotui, obtainPaotui, confirmPaotui, cancelPaotui, cancelObtainPaotui, finishPaotui, getPaotuiList, getMyPaotuiList, getMyObtainPaotuiList, updateErrand, removeErrands, getMyObtainMoney, getPayParams, getHSPayResult, getPayResult, getHsPays } from "./service/errands/";
import { getNotices, readNotice, getNoticeNum, removeNotices } from "./service/notices/";
import { sendMessage, getMessages } from "./service/messages/";
import { updateGood, getGoods, removeGoods, buyGood, getAllBuyGoods, getPoints, getLeftPoint, useLeftPoint } from "./service/goods/";
import { saveTransfer, doTransfer, getTransfers, removeTransfers } from  "./service/moneys/";
import { getAllStaticss } from "./service/statics/";
import { saveCollect, getCollects, removeCollects } from "./service/collects/";
import { saveReport, getReports, removeReports } from "./service/reports/";
import { saveBanner, getBanners, removeBanners } from "./service/banners/";

import auth from "../middleware/auth";
import { upload } from "./service/upload/";

// for multer middleware
import multer from "multer";
const storage = multer.diskStorage({
	destination: function (req, file, callback) {
		callback(null, "./public/uploads");
	},
	filename: function (req, file, callback) {
		callback(null, Date.now() + "-" + file.originalname);
	}
});

const type = multer({ storage }).single("img");
const router = Express.Router();

// file upload related api
router.post("/upload", type, upload);

// school related api
// router.post("/getSchools", auth);
router.post("/getSchools", getSchools);

router.post("/getSchool", auth);
router.post("/getSchool", getSchool);

// router.post("/getAllSchools", auth);
router.post("/getAllSchools", getAllSchools);

router.post("/updateSchool", auth);
router.post("/updateSchool", updateSchool);

router.post("/removeSchools", auth);
router.post("/removeSchools", removeSchools);

// user related api
router.post("/adminlog", adminlog);
router.post("/log", log);
router.post('/getValidateCode', getValidateCode);
router.post('/reg', reg);

router.post("/updateUserInfo", auth);
router.post('/updateUserInfo', updateUserInfo);

router.post("/user", auth);
router.post('/user', user);

router.post("/watchUser", auth);
router.post('/watchUser', watchUser);

router.post("/getUsers", auth);
router.post("/getUsers", getUsers);

// router.post("/viewUserInfo", auth);
router.post("/viewUserInfo", viewUserInfo);

router.post("/getMyWatch", auth);
router.post("/getMyWatch", getMyWatch);

router.post("/getMyFan", auth);
router.post("/getMyFan", getMyFan);

router.post("/updateUser", auth);
router.post("/updateUser", updateUser);

router.post("/removeUsers", auth);
router.post("/removeUsers", removeUsers);

router.post("/getAllUsers", auth);
router.post("/getAllUsers", getAllUsers);

router.post("/chargeUser", auth);
router.post("/chargeUser", chargeUser);

router.post("/getServiceUser", auth);
router.post("/getServiceUser", getServiceUser);

// router.post("/getCaptcha", auth);
router.post("/getCaptcha", getCaptcha);

// router.post("/resetPasswd", auth);
router.post("/resetPasswd", resetPasswd);

router.post("/dailySignin", auth);
router.post("/dailySignin", dailySignin);

// router.post("/getLoginValidateCode", auth);
router.post("/getLoginValidateCode", getLoginValidateCode);

// router.post("/validateCaptcha", auth);
router.post("/validateCaptcha", validateCaptcha);

// post related api
// router.post("/getPosts", auth);
router.post("/getPosts", getPosts);

// router.post("/getPostMsgs", auth);
router.post("/getPostMsgs", getPostMsgs);

// router.post("/getActivities", auth);
router.post("/getActivities", getActivities);

router.post("/joinActivity", auth);
router.post("/joinActivity", joinActivity);

router.post("/getPost", auth);
router.post("/getPost", getPost);

router.post("/createPost", auth);
router.post("/createPost", createPost);

router.post("/updatePost", auth);
router.post("/updatePost", updatePost);

router.post("/setTopPost", auth);
router.post("/setTopPost", setTopPost);

router.post("/noPassPost", auth);
router.post("/noPassPost", noPassPost);

router.post("/removePosts", auth);
router.post("/removePosts", removePosts);

router.post("/removePost", auth);
router.post("/removePost", removePost);

router.post("/likePost", auth);
router.post("/likePost", likePost);

router.post("/removePostMsgs", auth);
router.post("/removePostMsgs", removePostMsgs);

router.post("/cmtPost", auth);
router.post("/cmtPost", cmtPost);

// router.post("/sharePost", auth);
router.post("/sharePost", sharePost);

router.post("/unWatchPost", auth);
router.post("/unWatchPost", unWatchPost);

// router.post("/getMorePostMsgs", auth);
router.post("/getMorePostMsgs", getMorePostMsgs);

// router.post("/getAllPostLikes", auth);
router.post("/getAllPostLikes", getAllPostLikes);

router.post("/getMyLikePosts", auth);
router.post("/getMyLikePosts", getMyLikePosts);

router.post("/getMyWatchPost", auth);
router.post("/getMyWatchPost", getMyWatchPost);

router.post("/addCourse", auth);
router.post("/addCourse", addCourse);

router.post("/cmtCourse", auth);
router.post("/cmtCourse", cmtCourse);

// router.post("/searchPosts", auth);
router.post("/searchPosts", searchPosts);

// router.post("/viewPost", auth);
router.post("/viewPost", viewPost);

router.post("/likePostMsg", auth);
router.post("/likePostMsg", likePostMsg);

router.post("/removePostMsgs", auth);
router.post("/removePostMsgs", removePostMsgs);

// router.post("/getBest10List", auth);
router.post("/getBest10List", getBest10List);



// user2User related api
router.post("/getUser2Users", auth);
router.post("/getUser2Users", getUser2Users);

router.post("/getUser2User", auth);
router.post("/getUser2User", getUser2User);

router.post("/updateUser2User", auth);
router.post("/updateUser2User", updateUser2User);

router.post("/removeUser2Users", auth);
router.post("/removeUser2Users", removeUser2Users);

// secondhand related api
// router.secondhand("/getSecondhands", auth);
router.post("/getSecondhands", getSecondhands);

router.post("/getMySecondhands", auth);
router.post("/getMySecondhands", getMySecondhands);

router.post("/getMyObtainSecondhands", auth);
router.post("/getMyObtainSecondhands", getMyObtainSecondhands);

// router.post("/getSecondhand", auth);
router.post("/getSecondhand", getSecondhand);

router.post("/createSecondhand", auth);
router.post("/createSecondhand", createSecondhand);

router.post("/obtainSecondhand", auth);
router.post("/obtainSecondhand", obtainSecondhand);

router.post("/removeSecondhands", auth);
router.post("/removeSecondhands", removeSecondhands);

router.post("/likeSecondhand", auth);
router.post("/likeSecondhand", likeSecondhand);

router.post("/cmtSecondhand", auth);
router.post("/cmtSecondhand", cmtSecondhand);

// router.post("/shareSecondhand", auth);
router.post("/shareSecondhand", shareSecondhand);

// router.post("/getMoreSecondhandMsgs", auth);
router.post("/getMoreSecondhandMsgs", getMoreSecondhandMsgs);

// router.post("/searchSecondhands", auth);
router.post("/searchSecondhands", searchSecondhands);

// router.post("/viewSecondhand", auth);
router.post("/viewSecondhand", viewSecondhand);

// errand related api
// router.errand("/getErrands", auth);
router.post("/getErrands", getErrands);

// router.post("/getErrand", auth);
router.post("/getErrand", getErrand);

router.post("/createPaotui", auth);
router.post("/createPaotui", createPaotui);

router.post("/getPayParams", auth);
router.post("/getPayParams", getPayParams);

// router.post("/getHSPayResult", auth);
router.post("/getHSPayResult", getHSPayResult);

router.post("/getPayResult", auth);
router.post("/getPayResult", getPayResult);

router.post("/obtainPaotui", auth);
router.post("/obtainPaotui", obtainPaotui);

router.post("/confirmPaotui", auth);
router.post("/confirmPaotui", confirmPaotui);

router.post("/cancelPaotui", auth);
router.post("/cancelPaotui", cancelPaotui);

router.post("/cancelObtainPaotui", auth);
router.post("/cancelObtainPaotui", cancelObtainPaotui);

router.post("/finishPaotui", auth);
router.post("/finishPaotui", finishPaotui);

// router.post("/getPaotuiList", auth);
router.post("/getPaotuiList", getPaotuiList);

router.post("/getMyPaotuiList", auth);
router.post("/getMyPaotuiList", getMyPaotuiList);

router.post("/getMyObtainPaotuiList", auth);
router.post("/getMyObtainPaotuiList", getMyObtainPaotuiList);

router.post("/getMyObtainMoney", auth);
router.post("/getMyObtainMoney", getMyObtainMoney);

// router.post("/updateErrand", auth);
// router.post("/updateErrand", updateErrand);

router.post("/removeErrands", auth);
router.post("/removeErrands", removeErrands);

// router.post("/getHsPays", auth);
router.post("/getHsPays", getHsPays);

// notice related api
router.post("/getNotices", auth);
router.post("/getNotices", getNotices);

router.post("/readNotice", auth);
router.post("/readNotice", readNotice);

router.post("/getNoticeNum", auth);
router.post("/getNoticeNum", getNoticeNum);

router.post("/removeNotices", auth);
router.post("/removeNotices", removeNotices);

// doc related api
router.post("/getDocs", auth);
router.post("/getDocs", getDocs);

router.post("/updateDoc", auth);
router.post("/updateDoc", updateDoc);

router.post("/removeDocs", auth);
router.post("/removeDocs", removeDocs);

router.post("/removeDocs", auth);
router.post("/getAllDocs", getAllDocs);

// msg related api
router.post("/sendMessage", auth);
router.post("/sendMessage", sendMessage);

router.post("/getMessages", auth);
router.post("/getMessages", getMessages);

// good related api
router.post("/updateGood", auth);
router.post("/updateGood", updateGood);

router.post("/getGoods", auth);
router.post("/getGoods", getGoods);

router.post("/removeGoods", auth);
router.post("/removeGoods", removeGoods);

router.post("/buyGood", auth);
router.post("/buyGood", buyGood);

router.post("/getAllBuyGoods", auth);
router.post("/getAllBuyGoods", getAllBuyGoods);

router.post("/getPoints", auth);
router.post("/getPoints", getPoints);

router.post("/getLeftPoint", auth);
router.post("/getLeftPoint", getLeftPoint);

router.post("/useLeftPoint", auth);
router.post("/useLeftPoint", useLeftPoint);

// money related api

router.post("/saveTransfer", auth);
router.post("/saveTransfer", saveTransfer);

router.post("/doTransfer", auth);
router.post("/doTransfer", doTransfer);

router.post("/getTransfers", auth);
router.post("/getTransfers", getTransfers);

router.post("/removeTransfers", auth);
router.post("/removeTransfers", removeTransfers);

// statics related api
router.post("/getAllStaticss", auth);
router.post("/getAllStaticss", getAllStaticss);

// collect related api
router.post("/saveCollect", auth);
router.post("/saveCollect", saveCollect);

router.post("/getCollects", auth);
router.post("/getCollects", getCollects);

router.post("/removeCollects", auth);
router.post("/removeCollects", removeCollects);

// report related api
router.post("/saveReport", auth);
router.post("/saveReport", saveReport);

router.post("/getReports", auth);
router.post("/getReports", getReports);

router.post("/removeReports", auth);
router.post("/removeReports", removeReports);

// banner related api
router.post("/saveBanner", auth);
router.post("/saveBanner", saveBanner);

// router.post("/getBanners", auth);
router.post("/getBanners", getBanners);

router.post("/removeBanners", auth);
router.post("/removeBanners", removeBanners);

export default router;