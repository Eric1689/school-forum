export function getPayParams (req, res, next) {
    const { channel_id, occur_balance, prod_name, summary } = req.body;
    const dateString = moment().format('YYYYMMDD HHmmss');
    const [partner_trans_date, partner_trans_time] = dateString.split(' ');
    const data = {
        partner_id: "40000039",
        partner_serial_no: partner_trans_date+partner_trans_time+Math.random().toString(10).substr(2,8),
        partner_trans_date, partner_trans_time, 
        prod_name,
        currency_code: "CNY",
        occur_balance, 
        summary,
        channel_id, 
        receive_url: "http://111.231.87.118:3001/api/getHSPayResult",
    }
    // console.log(qs.stringify(data, { encode: false }))
    // const formData  = new FormData();
    // formData.append('partner_id', data.partner_id)
    // formData.append('partner_serial_no', data.partner_serial_no)
    // formData.append('partner_trans_date', data.partner_trans_date)
    // formData.append('partner_trans_time', data.partner_trans_time)
    // formData.append('prod_name', data.prod_name)
    // formData.append('currency_code', data.currency_code)
    // formData.append('occur_balance', data.occur_balance)
    // formData.append('summary', data.summary)
    // formData.append('channel_id', data.channel_id)
    // formData.append('receive_url', data.receive_url)
    
    // fetch('http://118.178.245.103:6101/YunPayDemo1.0/epay_wallet_pay.htm', { method: 'POST', body: formData })
    // .then(res => res.json())
    // .then(json => {
    //     console.log(json)
    //     return res.status(200).json({ code: 0, data:json });
    // });
    // fetch('http://118.178.245.103:6101/YunPayDemo1.0/epay_wallet_pay.htm?'+qs.stringify(data, { encode: true }))
    const msg = qs.stringify(data, { encode: false, charset: 'utf-8', sort: (a, b)=>a.localeCompare(b) });
    const signSHA = SHA256(msg).toString().toUpperCase()
    const sign = crypto.createSign('RSA-SHA1');
    sign.update(signSHA);
    const cert_sign = sign.sign(constantObj.privateKey, 'base64');
    data.cert_sign = cert_sign;
    console.log('msg', msg);
    console.log('signSHA', signSHA);
    console.log('privateKey', constantObj.privateKey);
    console.log('cert_sign', cert_sign);

    const formData  = new FormData();
    formData.append('partner_id', data.partner_id)
    formData.append('partner_serial_no', data.partner_serial_no)
    formData.append('partner_trans_date', data.partner_trans_date)
    formData.append('partner_trans_time', data.partner_trans_time)
    formData.append('prod_name', data.prod_name)
    formData.append('currency_code', data.currency_code)
    formData.append('occur_balance', data.occur_balance)
    formData.append('summary', data.summary)
    formData.append('channel_id', data.channel_id)
    formData.append('receive_url', data.receive_url)
    formData.append('cert_sign', data.cert_sign)

    // fetch('http://uat.epay.hscloud.cn:6101/eis/yunpay/epay_wallet_pay?'+qs.stringify(data, { encode: true }))
    fetch('http://uat.epay.hscloud.cn:6101/eis/yunpay/epay_wallet_pay', {method: 'POST', body:formData})
    .then(res => {
        console.log(res)
        return res.json()})
    .then(json => {
        // console.log(json)
        return res.status(200).json({ code: 0, data:json });
    });
}