import Good from '../../../Models/good';
import User from '../../../Models/user';
import BuyGood from '../../../Models/buygood';
import Point from '../../../Models/point';
import { getCreator, getLottery } from '../../../utils/util';

const uuidv1 = require('uuid/v1');
const goodEntity = new Good();
const userEntity = new User();
const buyGoodEntity = new BuyGood();
const pointEntity = new Point();

export function getGoods(req, res, next) {
    const {  ...query } = req.body;
    goodEntity.getGoods(query, (err, goods, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: goods, total, hasMore } });
    })
}

export function getGood(req, res, next) {
    const {  ...query } = req.body;
    goodEntity.getGood(query, (err, goods) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json(goods);
    })
}

export function getAllGoods(req, res, next) {
    goodEntity.getAllGoods({}, (err, goods) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: goods});
    })
}

export function updateGood(req, res, next) {
    const {  ...good } = req.body;
    const goodEntity = new Good();
    if (!good._id) {
        goodEntity.saveGood(good, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    } else {
        goodEntity.updateGood(good, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    }
}

export function removeGoods(req, res, next) {
    const goodIds = req.body.goodIds;

    goodEntity.removeGoods(goodIds, (err, good) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function likeGood(req, res, next) {
    const {  goodId, user } = req.body;
    goodEntity.getGood({ _id: goodId }, (err, good) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const { likes = {} } = good;
        if (likes[user._id]) {
            delete likes[user._id];
        } else {
            likes[user._id] = user;
        }
        good.likes = likes;
        goodEntity.updateGood(good, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}

export function cmtGood(req, res, next) {
    const {  goodId, user, cmt } = req.body;
    goodEntity.getGood({ _id: goodId }, (err, good) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const { cmts = [] } = good;
        cmts.push({ ...user, cmt })
        goodEntity.updateGood(good, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}

export function buyGood(req, res, next) {
    const { goodId } = req.body;
    const creator = getCreator(req.user);
    goodEntity.getGood({ _id: goodId }, (err, good) => {
        userEntity.getUser({ _id: creator._id }, (err, user) => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            if (good.point > (user.point || 0)) {
                return res.status(200).json({ code: 1, msg: '积分不够' });
            }
            const newUser = {
                _id: creator._id,
                point: user.point - good.point
            }
            userEntity.updateUser(newUser, err => {
                if (err) {
                    return res.status(200).json({ code: 1, msg: '服务器开小差' });
                }
                const buyGood = {
                    buyGoodId: good._id,
                    name: good.name,
                    point: good.point,
                    desc: good.desc,
                    url: good.url,
                    code: uuidv1().slice(0,13),
                    getterId: creator._id
                }
                const pointEntity = new Point();
                pointEntity.savePoint({creatorId: creator._id, score: good.point, desc: `购买“${good.name}”`},err=>{})
                buyGoodEntity.saveBuyGood(buyGood, (err, newBG)=>{
                    if (err) {
                        return res.status(200).json({ code: 1, msg: '服务器开小差' });
                    }
                    return res.status(200).json({ code: 0, data: newBG[0] });
                })
            });
        })
    })
}

export function getAllBuyGoods(req, res, next) {
    const creator = getCreator(req.user);
    buyGoodEntity.getAllBuyGoods({getterId: creator._id}, (err, buyGoods)=>{
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: buyGoods });
    })
}

export function getPoints(req, res, next) {
    const {  ...query } = req.body;
    const creator = getCreator(req.user);
    pointEntity.getPoints({creatorId: creator._id, ...query}, (err, points, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: points, total, hasMore } });
    })
}

export function getLeftPoint(req, res, next) {
    const creator = getCreator(req.user);
    userEntity.getUser({ _id: creator._id }, (err, user) => {
        if (err || !user) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: user.leftPoint || 0 });
    })
}

export function useLeftPoint(req, res, next) {
    const creator = getCreator(req.user);
    userEntity.getUser({ _id: creator._id }, (err, user) => {
        if (err || !user) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        if ((user.leftPoint || 0)<100) {
            return res.status(200).json({ code: 1, msg: '积分不足' });
        }
        const {text, ...uUser} = getLottery(creator._id, user);
        userEntity.updateUser(uUser, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0, data: text });
        });
    })
}

