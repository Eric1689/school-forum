import Report from '../../../Models/report';
import { saveNotice } from '../utils/';
const reportEntity = new Report();

export function getReports(req, res, next) {
    const { ...query } = req.body;
    reportEntity.getReports(query, (err, reports, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: reports, total, hasMore } });
    })
}

export function getReport(req, res, next) {
    const {  ...query } = req.body;
    reportEntity.getReport(query, (err, reports) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json(reports);
    })
}

export function saveReport(req, res, next) {
    const { ...report } = req.body;
    const reportEntity = new Report();
    reportEntity.saveReport(report, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const { beReported = {} } = report;
        const notice = {
            receiverId: beReported._id,
            receiver: beReported,
            url: '',
            msg: `有人举报了您`,
            type: 'system',
            status: 0,
        }
        saveNotice(notice);
        return res.status(200).json({ code: 0 });
    });
}

// export function updateReport(req, res, next) {
//     const {  ...report } = req.body;
//     const reportEntity = new Report();
//     if (!report._id) {
//         reportEntity.saveReport(report, err => {
//             if (err) {
//                 return res.status(200).json({ code: 1, msg: '服务器开小差' });
//             }
//             return res.status(200).json({ code: 0 });
//         });
//     } else {
//         reportEntity.updateReport(report, err => {
//             if (err) {
//                 return res.status(200).json({ code: 1, msg: '服务器开小差' });
//             }
//             return res.status(200).json({ code: 0 });
//         });
//     }
// }

export function removeReports(req, res, next) {
    const reportIds = req.body.reportIds;

    reportEntity.removeReports(reportIds, (err) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}