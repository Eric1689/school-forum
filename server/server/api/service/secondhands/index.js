import Secondhand from '../../../Models/secondhand';
import PostMsg from '../../../Models/postMsg';
import PostLike from '../../../Models/postLike';
import Notice from '../../../Models/notice';
import { getCreator } from '../../../utils/util';
const secondhandEntity = new Secondhand();
const postMsgEntity = new PostMsg();
const postLikeEntity = new PostLike();

export function getSecondhands(req, res, next) {
    const {  ...query } = req.body;
    // query.createTime = { $lt: query.createTime }
    query.status = 0;
    secondhandEntity.getSecondhands(query, (err, secondhands, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: secondhands, total, hasMore } });
    })
}

export function getMySecondhands(req, res, next) {
    const {  ...query } = req.body;
    // query.createTime = { $lt: query.createTime }
    const creator = getCreator(req.user)
    query.creatorId = creator._id;
    secondhandEntity.getSecondhands(query, (err, secondhands, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: secondhands, total, hasMore } });
    })
}

export function getMyObtainSecondhands(req, res, next) {
    const {  ...query } = req.body;
    // query.createTime = { $lt: query.createTime }
    const creator = getCreator(req.user)
    query.opponentId = creator._id;
    secondhandEntity.getSecondhands(query, (err, secondhands, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: secondhands, total, hasMore } });
    })
}

export function getSecondhand(req, res, next) {
    const {  ...query } = req.body;
    secondhandEntity.getSecondhand(query, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const data = secondhand._doc;
        postMsgEntity.getPostMsgs({postId: data._id, current: 1, pageSize: 100}, (err, cmts, count, hasMore) => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            postLikeEntity.getAllPostLikes({postId: data._id}, (err, likes) => {
                if (err) {
                    return res.status(200).json({ code: 1, msg: '服务器开小差' });
                }
                data.hasMore = hasMore;
                data.likes = likes;
                return res.status(200).json({ code: 0, data });
            })
        })
    })
}

export function getMoreSecondhandMsgs(req, res, next) {
    const {  ...query } = req.body;
    postMsgEntity.getPostMsgs(query, (err, cmts, count, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const data = {
            cmts, count, hasMore
        }
        return res.status(200).json({ code: 0, data });
    })
}

export function createSecondhand(req, res, next) {
    const {  ...secondhand } = req.body;
    const secondhandEntity = new Secondhand();
    const creator = getCreator(req.user)
    secondhand.creator = creator;
    secondhandEntity.saveSecondhand(secondhand, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function obtainSecondhand(req, res, next) {
    const {  _id } = req.body;
    const creator = getCreator(req.user);
    const secondhand = {_id, status: 1, opponent: creator, opponentId: creator._id}
    
    secondhandEntity.updateSecondhand(secondhand, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function updateSecondhand(req, res, next) {
    const {  ...secondhand } = req.body;
    const secondhandEntity = new Secondhand();
    if (!secondhand._id) {
        secondhandEntity.saveSecondhand(secondhand, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    } else {
        secondhandEntity.updateSecondhand(secondhand, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    }
}

export function removeSecondhands(req, res, next) {
    const secondhandIds = req.body.secondhandIds;

    secondhandEntity.removeSecondhands(secondhandIds, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function likeSecondhand(req, res, next) {
    let {  secondhandId } = req.body;
    const creator = getCreator(req.user)
    secondhandEntity.getSecondhand({ _id: secondhandId }, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        postLikeEntity.getPostLike({postId, creatorId: creator._id}, (err, like) => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            if (like) {
                return res.status(200).json({ code: 1, msg: '已经点过赞啦' });
            }
            const newLike = {
                postId,
                creatorId: creator._id,
                creator
            }
            postLikeEntity.savePostLike(newLike, err => {
                if (err) {
                    return res.status(200).json({ code: 1, msg: '服务器开小差' });
                }
                const data = secondhand._doc;
                const notice = {
                    receiverId: secondhand.creator._id,
                    url: '/detail/'+postId,
                    msg: `有人赞了您的二手货《${secondhand.content}》`,
                    type: 'like'
                }
                const noticeEntity = new Notice();
                noticeEntity.saveNotice(notice, err => {});
                const likeNum = (data.likeNum || 0) + 1;
                secondhandEntity.updateSecondhand({_id: data._id, likeNum}, err => {
                    if (err) {
                        return res.status(200).json({ code: 1, msg: '服务器开小差' });
                    }
                    return res.status(200).json({ code: 0 });
                });
            });
        })
    })
}

export function cmtSecondhand(req, res, next) {
    let {  secondhandId, cmt } = req.body;
    const creator = getCreator(req.user)
    secondhandEntity.getSecondhand({ _id: secondhandId }, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const data = secondhand._doc;
        // post and secondhand use the same msg table
        postMsgEntity.savePostMsg({content: cmt,postId: secondhandId}, (err, cmts) => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            data.cmtNum = (data.cmtNum || 0) + 1;
            secondhandEntity.updateSecondhand(data, err => {
                if (err) {
                    return res.status(200).json({ code: 1, msg: '服务器开小差' });
                }
                const notice = {
                    receiverId: data.creator._id,
                    creator: data.creator,
                    url: '/detail/'+postId,
                    msg: `有人评论了您的二手《${data.content}》`,
                    type: 'cmt'
                }
                const noticeEntity = new Notice();
                noticeEntity.saveNotice(notice, err => {
                    // if (err) {
                    //     return res.status(200).json({ code: 1, msg: '服务器开小差' });
                    // }
                    // return res.status(200).json({ code: 0 });
                });
                return res.status(200).json({ code: 0 });
            });
        })
    })
}

export function shareSecondhand(req, res, next) {
    let {  secondhandId } = req.body;

    secondhandEntity.getSecondhand({ _id: secondhandId }, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        secondhand.shareNum = (shareNum || 0) + 1;
        secondhandEntity.updateSecondhand(secondhand, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}

export function searchSecondhands(req, res, next) {
    const { keyword } = req.body;
    const query = {"$or":[{
        title: {
            $regex: keyword,
            $options: "i"
          }
    }, {
        content: {
            $regex: keyword,
            $options: "i"
          }
    }]};

    secondhandEntity.getSecondhands(query, (err, posts, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: posts, total, hasMore } });
    })
}

export function viewSecondhand(req, res, next) {
    let {  secondhandId } = req.body;

    secondhandEntity.getSecondhand({ _id: secondhandId }, (err, secondhand) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        secondhand.viewNum = (secondhand.viewNum || 0) + 1;
        secondhandEntity.updateSecondhand(secondhand, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}
