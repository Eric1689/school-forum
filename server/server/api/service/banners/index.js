import Banner from '../../../Models/banner';
const bannerEntity = new Banner();

export function saveBanner(req, res, next) {
    const {  ...banner } = req.body;
    const bannerEntity = new Banner();
    bannerEntity.saveBanner(banner, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function getBanners(req, res, next) {
    const {  ...query } = req.body;
    bannerEntity.getAllBanners(query, (err, banners) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: banners });
    })
}

export function removeBanners(req, res, next) {
    const bannerIds = req.body.bannerIds;

    bannerEntity.removeBanners(bannerIds, (err, banner) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}