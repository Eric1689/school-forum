import Money from '../../../Models/money';
import { getCreator } from '../../../utils/util';
const moneyEntity = new Money();

export function saveTransfer(req, res, next) {
    const {  ...money } = req.body;
    const moneyEntity = new Money();
    moneyEntity.saveMoney(money, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function doTransfer(req, res, next) {
    const {  ...money } = req.body;
    money.isDone = 1;
    moneyEntity.updateMoney(money, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function getTransfers(req, res, next) {
    const creator = getCreator(req.user);
    creatorId = creator._id;
    const query = {"$or":[{fromUserId: creatorId}, {toUserId: creatorId}]};
    moneyEntity.getMoneys(query, (err, moneys, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: moneys, total, hasMore } });
    })
}

export function removeTransfers(req, res, next) {
    const moneyIds = req.body.moneyIds;

    moneyEntity.removeMoneys(moneyIds, (err, money) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}