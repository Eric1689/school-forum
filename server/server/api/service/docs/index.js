import qs from 'qs';
import Doc from '../../../Models/doc';
const fs = require('fs');
const docEntity = new Doc();

export function getDocs(req,res,next){
	const { ...query} = req.body;
    docEntity.getDocs(query,(err, docs, total)=>{
        if(err){
            return res.status(500).end('服务器错误');
        }
        return res.status(200).json({docs, total});
    })
}

export function getDoc(req,res,next){
    docEntity.getDoc({},(err,doc)=>{
        if(err){
            return res.status(500).end('服务器错误');
        }
        return res.status(200).json(doc);
    })
}

export function updateDoc(req,res,next){
    const { ...doc} = req.body;
	const docEntity = new Doc();
	if(!doc._id) {
		docEntity.saveDoc(doc,err=>{
            if(err){
                return res.status(500).end()
            }
            return res.status(200).json({ok:1});
        })
	} else {
		docEntity.updateDoc(doc,err=>{
            if(err){
                return res.status(500).end();
            }
            return res.status(200).json({ok:1});
        })
	}
}

export function removeDocs(req,res,next){
    const docIds = req.body.docIds;
    const docEntity = new Doc();

    docEntity.removeDocs(docIds,(err,doc)=>{
        if(err){
            return res.status(500).end('服务器错误');
        }
        return res.status(200).json({ok:1});
    });
}

export function getAllDocs(req,res,next){
    docEntity.getAllDocs({},(err,docs)=>{
        if(err){
            return res.status(500).end('服务器错误');
        }
        return res.status(200).json(docs);
    });
}

export function uploadDocs(req,res,next){
    // need to remove public prefix;
    // const fileInfo = path.parse(req.file.path);
    const fileString = fs.readFileSync(req.file.path, 'utf8');
    return res.status(200).json({status:'success', url: req.file.path.slice(6)});
}
