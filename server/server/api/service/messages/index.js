import Message from '../../../Models/message';
import Notice from '../../../Models/notice';
import { getCreator } from '../../../utils/util';
const messageEntity = new Message();
const noticeEntity = new Notice();

export function getMessages(req, res, next) {
    const { receiverId } = req.body;
    const senderId = req.user._id;
    const query = {"$or":[{senderId, receiverId}, {senderId: receiverId, receiverId: senderId}]};
    messageEntity.getMessages(query, (err, messages, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: messages, total, hasMore } });
    })
}

export function getMessage(req, res, next) {
    const {  ...query } = req.body;
    messageEntity.getMessage(query, (err, messages) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json(messages);
    })
}

export function sendMessage(req, res, next) {
    const { receiver, msg } = req.body;
    const messageEntity = new Message();
    const creator = getCreator(req.user);
    const senderId = creator._id;
    const message = {senderId, sender: creator, receiverId: receiver._id, receiver, msg};
    messageEntity.saveMessage(message, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        noticeEntity.getNotice({type: 'chat', receiverId: receiver._id}, (err, notice)=>{
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            if (!notice) {
                const noticeEntity = new Notice();
                const notice = {
                    sender: creator,
                    receiverId: receiver._id,
                    receiver,
                    url: '/chat/'+receiver._id,
                    msg: `${req.user.nickname}给您发了新消息`,
                    type: 'chat',
                    status: 0,
                }
                noticeEntity.saveNotice(notice, err => {
                    if (err) {
                        return res.status(200).json({ code: 1, msg: '服务器开小差' });
                    }
                    return res.status(200).json({ code: 0 });
                });
            } else {
                if (notice.status === 1) {
                    noticeEntity.updateNotice({_id: notice._id.toString(), status: 0}, err => {
                        if (err) {
                            return res.status(200).json({ code: 1, msg: '服务器开小差' });
                        }
                        return res.status(200).json({ code: 0 });
                    });
                }
                return res.status(200).json({ code: 0 });
            }
        });
    });
}

// export function updateMessage(req, res, next) {
//     const {  ...message } = req.body;
//     const messageEntity = new Message();
//     if (!message._id) {
//         messageEntity.saveMessage(message, err => {
//             if (err) {
//                 return res.status(200).json({ code: 1, msg: '服务器开小差' });
//             }
//             return res.status(200).json({ code: 0 });
//         });
//     } else {
//         messageEntity.updateMessage(message, err => {
//             if (err) {
//                 return res.status(200).json({ code: 1, msg: '服务器开小差' });
//             }
//             return res.status(200).json({ code: 0 });
//         });
//     }
// }

export function removeMessages(req, res, next) {
    const messageIds = req.body.messageIds;

    messageEntity.removeMessages(messageIds, (err, message) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}