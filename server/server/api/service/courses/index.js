import qs from 'qs';
import Course from '../../../Models/course';
import CourseMsg from '../../../Models/courseMsg';
import { getCreator } from '../../../utils/util';
const courseEntity = new Course();
const courseMsgEntity = new CourseMsg();

export function getCourses(req, res, next) {
    const {  ...query } = req.body;
    courseEntity.getCourses(query, (err, courses, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: courses, total, hasMore } });
    })
}

export function getCourse(req, res, next) {
    const {  ...query } = req.body;
    courseEntity.getCourse(query, (err, courses) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: courses});
    })
}

export function getAllCourses(req, res, next) {
    courseEntity.getAllCourses({}, (err, courses) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: courses });
    })
}

export function addCourse(req, res, next) {
    const {  ...course } = req.body;
    const creator = getCreator(req.user);
    const courseEntity = new Course();
    const { courseName, teacher, tags, msg, score } = course;
    const user = req.user;
    course.tags = tags.reduce((t,c)=>{t[c]=true;return t}, {});
    course.score = score;
    course.cmtNumber = 1;
    course.creator = creator;
    const courseMsg = {
        tags, msg, score, like: {}
    }
    courseEntity.getCourse({courseName, teacher}, (err, courses) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        if (courses) {
            return res.status(200).json({ code: 1, msg: '该课程已存在' });
        }

        const courseMsgEntity = new CourseMsg();

        courseEntity.saveCourse(course, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            courseMsg.courseId = course._id;
            courseMsgEntity.saveCourseMsg(courseMsg, err => {
                if (err) {
                    return res.status(200).json({ code: 1, msg: '服务器开小差' });
                }
                return res.status(200).json({ code: 0 });
            });
        });
    })
}

export function updateCourse(req, res, next) {
    const {  ...course } = req.body;
    const { tags, msg, score } = course;
    const user = req.user;
    course.tags = tags.reduce((t,c)=>{t.c=true;return t}, {})
    course.score = score.reduce((t,c)=>{
        const [c1, c2, c3, c4, c5] = c;
        t[0] = (t[0]*cmtNumber+c1)/(cmtNumber+1);
        t[1] = (t[1]*cmtNumber+c2)/(cmtNumber+1);
        t[2] = (t[2]*cmtNumber+c3)/(cmtNumber+1);
        t[3] = (t[3]*cmtNumber+c4)/(cmtNumber+1);
        t[4] = (t[4]*cmtNumber+c5)/(cmtNumber+1);
    },[]);
    course.cmtNumber = course.cmtNumber + 1
    courseEntity.updateCourse(course, err => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function removeCourses(req, res, next) {
    const courseIds = req.body.courseIds;
    const courseEntity = new Course();

    courseEntity.removeCourses(courseIds, (err, course) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}