import School from '../../../Models/school';
const schoolEntity = new School();

export function getSchools(req, res, next) {
    const {  ...query } = req.body;
    schoolEntity.getSchools(query, (err, schools, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: schools, total, hasMore } });
    })
}

export function getSchool(req, res, next) {
    const {  ...query } = req.body;
    schoolEntity.getSchool(query, (err, schools) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json(schools);
    })
}

export function getAllSchools(req, res, next) {
    schoolEntity.getAllSchools({}, (err, schools) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: schools});
    })
}

export function updateSchool(req, res, next) {
    const {  ...school } = req.body;
    const schoolEntity = new School();
    if (!school._id) {
        schoolEntity.saveSchool(school, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    } else {
        schoolEntity.updateSchool(school, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    }
}

export function removeSchools(req, res, next) {
    const schoolIds = req.body.schoolIds;

    schoolEntity.removeSchools(schoolIds, (err, school) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}

export function likeSchool(req, res, next) {
    const {  schoolId, user } = req.body;
    schoolEntity.getSchool({ _id: schoolId }, (err, school) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const { likes = {} } = school;
        if (likes[user._id]) {
            delete likes[user._id];
        } else {
            likes[user._id] = user;
        }
        school.likes = likes;
        schoolEntity.updateSchool(school, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}

export function cmtSchool(req, res, next) {
    const {  schoolId, user, cmt } = req.body;
    schoolEntity.getSchool({ _id: schoolId }, (err, school) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const { cmts = [] } = school;
        cmts.push({ ...user, cmt })
        schoolEntity.updateSchool(school, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    })
}