import User2User from '../../../Models/user2User';
// import { getCreator } from '../../../utils/util';
const user2UserEntity = new User2User();

export function getMyWatch(req, res, next) {
    const creatorId = req.user._id;
    const query = { $or: [{userId: creatorId }, {user2Id: creatorId }] }
    user2UserEntity.getAllUser2Users(query, (err, user2Users) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const {watchList, watchedObj} = user2Users.reduce((t,c)=>{
            if (c.userId === creatorId) {
                t.watchList.push(c._doc)
            }
            if (c.user2Id === creatorId) {
                t.watchedObj[c.userId] = true
            }
            return t
        }, {
            watchList: [],
            watchedObj: {}
        })
        watchList.forEach(w=>{
            w.isMutual = !!watchedObj[w.user2Id]
        })
        return res.status(200).json({ code: 0, data: watchList });
    })
}

export function getMyFan(req, res, next) {
    const creatorId = req.user._id;
    const query = { $or: [{userId: creatorId }, {user2Id: creatorId }] }
    user2UserEntity.getAllUser2Users(query, (err, user2Users, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        const {fanList, fanObj} = user2Users.reduce((t,c)=>{
            if (c.user2Id === creatorId) {
                t.fanList.push(c._doc)
            }
            if (c.userId === creatorId) {
                t.fanObj[c.user2Id] = true
            }
            return t
        }, {
            fanList: [],
            fanObj: {}
        })
        fanList.forEach(w=>{
            w.isMutual = !!fanObj[w.userId]
        })
        return res.status(200).json({ code: 0, data: fanList });
    })
}

export function getUser2Users(req, res, next) {
    const {  ...query } = req.body;
    user2UserEntity.getUser2Users(query, (err, user2Users, total, hasMore) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0, data: { list: user2Users, total, hasMore } });
    })
}

export function getUser2User(req, res, next) {
    const {  ...query } = req.body;
    user2UserEntity.getUser2User(query, (err, user2Users) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json(user2Users);
    })
}

export function updateUser2User(req, res, next) {
    const {  ...user2User } = req.body;
    const user2UserEntity = new User2User();
    if (!user2User._id) {
        user2UserEntity.saveUser2User(user2User, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    } else {
        user2UserEntity.updateUser2User(user2User, err => {
            if (err) {
                return res.status(200).json({ code: 1, msg: '服务器开小差' });
            }
            return res.status(200).json({ code: 0 });
        });
    }
}

export function removeUser2Users(req, res, next) {
    const user2UserIds = req.body.user2UserIds;

    user2UserEntity.removeUser2Users(user2UserIds, (err, user2User) => {
        if (err) {
            return res.status(200).json({ code: 1, msg: '服务器开小差' });
        }
        return res.status(200).json({ code: 0 });
    });
}