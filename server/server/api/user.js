import User from '../Models/user';

export default function (req, res, next) {
    const user = req.user;
    const userEntity = new User();
    userEntity.getUser({
        name: user.name
    }, (err, user) => {
        if (user) {
            return res.status(200).json({ code: 0, data: user });
        } else {
            return res.status(401).end();
        }
    });
}