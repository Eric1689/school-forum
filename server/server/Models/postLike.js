// 帖子和二手可以共用这个评论
import mongoose from 'mongoose';
import moment from 'moment';
import db from './db';
mongoose.Promise = global.Promise;

const PostLikeSchema = new mongoose.Schema({
    postId: String, // 帖子id
    postContent: String, // 帖子内容
    postCreateTime: String, // 帖子内容
    type: Number, // 0 post, 1 activity, 2 vote,
    creatorId: String, // 点赞者id
    creator: Object, // 点赞者
    createTime: Number,
});

PostLikeSchema.methods.savePostLike = function (postLike, cb) {
    postLike.createTime = moment().unix();
    this.model('postLike').insertMany([postLike], cb);
};

PostLikeSchema.methods.getPostLikes = function (query, cb) {
    const { current, pageSize, ...leftQuery } = query;
    const postLikeModel = this.model('postLike');
    postLikeModel.count(leftQuery, function (err, count) {
        const hasMore = count / pageSize > current;
        postLikeModel.find(leftQuery).sort({createTime: -1}).skip((current - 1) * pageSize).limit(pageSize).exec((err, postLikes) => cb(err, postLikes, count, hasMore));
    });
};

PostLikeSchema.methods.getAllPostLikes = function (query, cb) {
    this.model('postLike').find(query, cb);
};

PostLikeSchema.methods.getPostLike = function (query, cb) {
    this.model('postLike').findOne(query, cb);
};

PostLikeSchema.methods.updatePostLike = function (postLike, cb) {
    const _id = postLike._id;
    delete postLike._id;
    this.model('postLike').findOneAndUpdate({ _id }, { $set: postLike }, cb);
};

PostLikeSchema.methods.removePostLikes = function (postLikeIds, cb) {
    this.model('postLike').remove({ _id: { $in: postLikeIds } }, cb);
};

export default db.model('postLike', PostLikeSchema);