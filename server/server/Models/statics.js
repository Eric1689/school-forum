import mongoose from 'mongoose';
import moment from 'moment';
import db from './db';
mongoose.Promise = global.Promise;

const StaticsSchema = new mongoose.Schema({
    dateString: String,
    type: Number, // 0:用户报表 1 财务收入报表 2 财务支出
    dailyValue: Number, // 每日增加值
    sum: Number, // 总值
    createTime: Number
});

StaticsSchema.methods.saveStatics = function (statics, cb) {
    statics.createTime = moment().unix();
    this.model('statics').insertMany([statics], cb);
};

StaticsSchema.methods.getStaticss = function (query, cb) {
    const { current = 1, pageSize = 10, ...leftQuery } = query;
    const staticsModel = this.model('statics');
    staticsModel.count(leftQuery, function (err, count) {
        const hasMore = count / pageSize > current;
        staticsModel.find(leftQuery, { passwd: 0 }).skip((current - 1) * pageSize).limit(pageSize).exec((err, staticss) => cb(err, staticss, count, hasMore));
    });
};

StaticsSchema.methods.getAllStaticss = function (query, cb) {
    this.model('statics').find(query, cb);
};

StaticsSchema.methods.getStatics = function (query, cb) {
    this.model('statics').findOne(query, cb);
};

StaticsSchema.methods.updateStatics = function (statics, cb) {
    const _id = statics._id;
    delete statics._id;
    this.model('statics').findOneAndUpdate({ _id }, { $set: statics }, cb);
};

StaticsSchema.methods.removeStaticss = function (staticsIds, cb) {
    this.model('statics').remove({ _id: { $in: staticsIds } }, cb);
};

export default db.model('statics', StaticsSchema);