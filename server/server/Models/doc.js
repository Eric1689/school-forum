import mongoose from 'mongoose';
import db from './db';
mongoose.Promise = global.Promise;

const DocSchema = new mongoose.Schema({
    // id system generate
    tel: String, // 用户电话
    name: String, // 用户称呼
    address: String, // 配送地址
    serviceDate: String, // 服务日期
    number: String, // 件数
    note: String, // 备注
    status: String, // 当前服务状态,
    backDate: String, // 送回时间
});

DocSchema.methods.saveDoc = function(doc,cb){
    this.tel = doc.tel;
    this.name = doc.name;
    this.address = doc.address;
    this.serviceDate = doc.serviceDate;
    this.number = doc.number;
    this.note = doc.note;
    this.status = doc.status;
    this.backDate = doc.backDate;
    this.save(cb);
};

DocSchema.methods.getDocs = function(query,cb){
    const {current, pageSize, ...leftQuery} = query;
    const docModal = this.model('doc');
    docModal.count(leftQuery,function(err,count){
        docModal.find(leftQuery).skip((current-1)*pageSize).limit(pageSize).exec((err, docs)=>cb(err, docs, count));
    });

};

DocSchema.methods.getDoc = function(query,cb){
    this.model('doc').findOne(query,cb);
};

DocSchema.methods.updateDoc = function(doc, cb){
    const _id = doc._id;
    delete doc._id;
    this.model('doc').findOneAndUpdate({_id}, { $set: doc}, cb);
};

DocSchema.methods.removeDocs = function(docIds, cb){
    this.model('doc').remove({_id: {$in: docIds}}, cb);
};

DocSchema.methods.getAllDocs = function(query,cb){
    // this.model('doc').find(query,{docName:1,_id: 1},cb);
    this.model('doc').find(query,{_id: 1, tags: 1, docName: 1, url: 1}, cb);
};

export default db.model('doc',DocSchema);
