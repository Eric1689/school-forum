const mongoose = require("mongoose");
import config from '../../config';
const db = mongoose.createConnection(`${config.dbaddr}:${config.dbport}/${config.db}?authSource=admin`,{user:config.dbuser,pass:config.dbpwd});
db.once('open',()=>{
});

export default db;
