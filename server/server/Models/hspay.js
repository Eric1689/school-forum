// 帖子和二手可以共用这个评论
import mongoose from 'mongoose';
import moment from 'moment';
import db from './db';
mongoose.Promise = global.Promise;

const HsPaySchema = new mongoose.Schema({
    targetId: String, // 目标ID
    channel_serial_no: String, // hs返回的序列id
    hsPayBack: Object, // 恒生返回的
    hsTuiSong: Object, // 恒生推送的
    createTime: Number,
});

HsPaySchema.methods.saveHsPay = function (hsPay, cb) {
    hsPay.createTime = moment().unix();
    this.model('hsPay').insertMany([hsPay], cb);
};

HsPaySchema.methods.getHsPays = function (query, cb) {
    const { current, pageSize, ...leftQuery } = query;
    const hsPayModel = this.model('hsPay');
    if (leftQuery.createTime) {
        leftQuery.createTime = { $lt: leftQuery.createTime }
    }
    hsPayModel.count(leftQuery, function (err, count) {
        const hasMore = count / pageSize > current;
        hsPayModel.find(leftQuery).sort({createTime: -1}).skip((current - 1) * pageSize).limit(pageSize).exec((err, hsPays) => cb(err, hsPays, count, hasMore));
    });
};

HsPaySchema.methods.getAllHsPays = function (query, cb) {
    this.model('hsPay').find(query, { name: 1, _id: 0 }, cb);
};

HsPaySchema.methods.getHsPay = function (query, cb) {
    this.model('hsPay').findOne(query, cb);
};

HsPaySchema.methods.updateHsPay = function (hsPay, cb) {
    const _id = hsPay._id;
    delete hsPay._id;
    this.model('hsPay').findOneAndUpdate({ _id }, { $set: hsPay }, cb);
};

HsPaySchema.methods.removeHsPays = function (hsPayIds, cb) {
    this.model('hsPay').remove({ _id: { $in: hsPayIds } }, cb);
};

export default db.model('hsPay', HsPaySchema);