import mongoose from 'mongoose';
import db from './db';
import moment from 'moment';
mongoose.Promise = global.Promise;

const ReportSchema = new mongoose.Schema({
    type: String, // 用户user，评论msg，帖子post，二手secondhand，跑腿errand
    targetId: String, // userId, msgId, postId等
    beReported: Object, // 被举报的人
    beReportedId: String,
    msg: String, // 举报者的话
    reportId: String,
    createTime: String
});

ReportSchema.methods.saveReport = function (report, cb) {
    report.createTime = moment().unix();
    this.model('report').insertMany([report], cb);
};

ReportSchema.methods.getReports = function (query, cb) {
    const { current = 1, pageSize = 10, ...leftQuery } = query;
    const reportModel = this.model('report');
    reportModel.count(leftQuery, function (err, count) {
        reportModel.find(leftQuery).skip((current - 1) * pageSize).limit(pageSize).exec((err, reports) => cb(err, reports, count));
    });
};

ReportSchema.methods.getAllReports = function (query, cb) {
    this.model('report').find(query, { name: 1, _id: 0 }, cb);
};

ReportSchema.methods.getReport = function (query, cb) {
    this.model('report').findOne(query, cb);
};

ReportSchema.methods.updateReport = function (report, cb) {
    const _id = report._id;
    delete report._id;
    this.model('report').findOneAndUpdate({ _id }, { $set: report }, cb);
};

ReportSchema.methods.removeReports = function (reportIds, cb) {
    this.model('report').remove({ _id: { $in: reportIds } }, cb);
};

export default db.model('report', ReportSchema);