export const getCreator = (user = {}) => {
  const { _id, avatar, nickname } = user;
  return { _id, avatar, nickname }
}

export const getLottery = (_id, user) => {
  const score = Number(Math.random().toFixed(2));
  if (score>0.98) {
    return {_id, text: '10元代金券', money: (user.money || 0) + 10, leftPoint: user.leftPoint-100}
  } else if (score>0.96) {
    return {_id, text: '5元代金券', money: (user.money || 0) + 5, leftPoint: user.leftPoint-100}
  }  else if (score>0.93) {
    return {_id, text: '3元代金券', money: (user.money || 0) + 3, leftPoint: user.leftPoint-100}
  }  else if (score>0.77) {
    return {_id, text: '谢谢参与', leftPoint: user.leftPoint-100}
  }  else if (score>0.47) {
    return {_id, text: '50积分', leftPoint: user.leftPoint-50}
  } else if (score>0.31) {
    return {_id, text: '谢谢参与', leftPoint: user.leftPoint-100}
  } else if (score>0.09) {
    return {_id, text: '150积分', leftPoint: user.leftPoint+50}
  } else {
    return {_id, text: '1元代金券', money: (user.money || 0) + 1, leftPoint: user.leftPoint-100}
  }
}

export const getLevel = (num = 0) => {
  if (num<150) {
    return 1;
  } else if (num<300) {
    return 2;
  } else if (num<480) {
    return 3;
  } else if (num<650) {
    return 4;
  } else if (num<850) {
    return 5;
  } else if (num<1100) {
    return 6;
  } else if (num<1380) {
    return 7;
  } else if (num<1600) {
    return 8;
  } else if (num<2000) {
    return 9;
  } else if (num<3000) {
    return 10;
  } else if (num<4500) {
    return 11;
  } else if (num<10000) {
    return 12;
  } else if (num<100000) {
    return 13;
  }
}