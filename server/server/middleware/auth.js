const jwt = require("jwt-simple");

export default function (req, res, next) {
    const { token } = req.headers;
    let user;
    if (token) {
        try {
            user = jwt.decode(token, req.app.get('jwtTokenSecret'));
            if (user.exp < Date.now()) {
                return res.status(401).end('token expired');
            }
            delete user.exp;
        } catch (err) {
            return res.status(401).end('token err');
        }
    } else {
        return res.status(401).end('no token');
    }
    req.user = user;
    next();
}
