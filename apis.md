ju## 1. 添加课程
* 地址

/api/addCourse 

* 入参
```
{
	"title": "张老师2",
	"content": "宏观经济学888",
	"cmt": "这可还是不错的，哈哈",
	"tags": [1, 2],
	"scores": [1, 2, 3, 4, 5]
}
```

* 出参

```
{
    "code": 0
}
```

## 2. 评论课程

* 地址

/api/cmtCourse

* 入参

```
{
	"postId": "5bcdfb4d8ea4f57ff7b8d1cb",
	"cmt": "我也来评论下",
	"tags": [2, 3],
	"scores": [3,3,3,3,3]
}
```

* 出参

```
{
    "code": 0
}
```

## 3. 获取课程列表

* 地址

/api/getPosts

* 入参

```
{
	"current": 1,
	"pageSize": 10,
	"type": 3
}
```

* 出参

```
{
    "code": 0,
    "data": {
        "list": [
            {
                "_id": "5bcdfb4d8ea4f57ff7b8d1cb",
                "__v": 0,
                "title": "张老师2",
                "content": "宏观经济学888",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "type": 3,
                "createTime": 1540225869,
                "scores": [
                    1,
                    2,
                    3,
                    4,
                    5
                ],
                "tags": [
                    1,
                    2
                ],
                "cmtNum": 1,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            },
            {
                "_id": "5bcdfad8737bf37fc537f74d",
                "__v": 0,
                "title": "张老师1",
                "content": "宏观经济学888",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "type": 3,
                "createTime": 1540225752,
                "scores": [
                    1,
                    2,
                    3,
                    4,
                    5
                ],
                "tags": [
                    1,
                    2
                ],
                "cmtNum": 1,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            },
            {
                "_id": "5bcdfaa6e86d0a7faa6a0de3",
                "__v": 0,
                "title": "张老师9",
                "content": "宏观经济学888",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "type": 3,
                "createTime": 1540225702,
                "scores": [
                    1,
                    2,
                    3,
                    4,
                    5
                ],
                "tags": [
                    1,
                    2
                ],
                "cmtNum": 1,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            },
            {
                "_id": "5bcdfa7bfb32777f80be7ccd",
                "__v": 0,
                "title": "张老师99",
                "content": "宏观经济学888",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "type": 3,
                "createTime": 1540225659,
                "scores": [
                    1,
                    2,
                    3,
                    4,
                    5
                ],
                "tags": [
                    1,
                    2
                ],
                "cmtNum": 1,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            },
            {
                "_id": "5bcdfa3084d3cb7f6c8b205e",
                "__v": 0,
                "title": "张老师",
                "content": "宏观经济学888",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "type": 3,
                "createTime": 1540225584,
                "scores": [
                    4.25,
                    3.625,
                    3,
                    2.375,
                    1.75
                ],
                "tags": [
                    2,
                    3,
                    1
                ],
                "cmtNum": 2,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            }
        ],
        "total": 5,
        "hasMore": false
    }
}
```


## 4. 获取课程详情
* 地址

/api/getPost 

* 入参
```
{
	"_id": "5bcdfb4d8ea4f57ff7b8d1cb"
}
```

* 出参

```
{
    "code": 0,
    "data": {
        "imgs": [],
        "likeNum": 0,
        "shareNum": 0,
        "cmtNum": 3,
        "tags": [
            2,
            3,
            1
        ],
        "scores": [
            3,
            3,
            3,
            3,
            3
        ],
        "createTime": 1540225869,
        "type": 3,
        "creator": {
            "nickname": "888",
            "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
            "_id": "5bb1fab6e4ce4e194f53edbc"
        },
        "creatorId": "5bb1fab6e4ce4e194f53edbc",
        "content": "宏观经济学888",
        "title": "张老师2",
        "__v": 0,
        "_id": "5bcdfb4d8ea4f57ff7b8d1cb",
        "cmts": [
            {
                "_id": "5bcdfbab8ea4f57ff7b8d1d3",
                "__v": 0,
                "content": "我也来评论下",
                "postId": "5bcdfb4d8ea4f57ff7b8d1cb",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "createTime": 1540225963,
                "tags": [
                    2,
                    3
                ],
                "scores": [
                    3,
                    3,
                    3,
                    3,
                    3
                ]
            },
            {
                "_id": "5bcdfb888ea4f57ff7b8d1d1",
                "__v": 0,
                "content": "我也来评论下",
                "postId": "5bcdfb4d8ea4f57ff7b8d1cb",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "createTime": 1540225928,
                "tags": [
                    2,
                    3
                ],
                "scores": [
                    5,
                    4,
                    3,
                    2,
                    1
                ]
            },
            {
                "_id": "5bcdfb4d8ea4f57ff7b8d1cd",
                "__v": 0,
                "content": "这可还是不错的，哈哈",
                "postId": "5bcdfb4d8ea4f57ff7b8d1cb",
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "createTime": 1540225869,
                "tags": [
                    1,
                    2
                ],
                "scores": [
                    1,
                    2,
                    3,
                    4,
                    5
                ]
            }
        ],
        "hasMore": false,
        "likes": []
    }
}
```

## 5. 查看用户个人主页
* 地址

/api/viewUserInfo

* 入参
```
{
	"userId": "5bcdfb4d8ea4f57ff7b8d1cd",
}
```

* 出参
  
  > 暂定返回5个参数

```
{
    "code": 0,
    "data": {
        "nickname": "888",
        "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
        "college": null,
        "grade": "小米",
        "posts": [
            {
                "_id": "5bcdc703ee032d6423f48465",
                "__v": 0,
                "content": "<pre>123456</pre>",
                "category": "信息",
                "type": 0,
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1540098688569-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "888"
                },
                "createTime": 1540212483,
                "scores": [],
                "tags": [],
                "cmtNum": 0,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            },
            {
                "_id": "5bcb5f9c1036373333475c57",
                "__v": 0,
                "content": "<pre>7777</pre>",
                "category": "社区话题",
                "type": 0,
                "creatorId": "5bb1fab6e4ce4e194f53edbc",
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://111.231.87.118:3001/uploads/1539841844194-Screen Shot 2018-07-18 at 2.57.20 PM.png",
                    "nickname": "1234"
                },
                "createTime": 1540054940,
                "scores": [],
                "tags": [],
                "cmtNum": 0,
                "shareNum": 0,
                "likeNum": 0,
                "imgs": []
            }
        ]
    }
}
```

## 6. 搜索帖子

* 地址

/api/searchPosts

* 入参

会搜索title和content，是否包含keyword。二手类似
```
{
	"keyword": "66"
}
```

* 出参

```
{
    "code": 0,
    "data": {
        "list": [
            {
                "_id": "5bb6d02509fd5310093b80ee",
                "__v": 0,
                "content": "666",
                "category": "社区话题",
                "type": 0,
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://www.hzic.edu.cn/UploadFiles/image/201809/2018090443662013.jpg",
                    "nickname": "财大小逗比"
                },
                "isAuth": true,
                "createTime": 1538707493,
                "isTop": false,
                "scores": [],
                "tags": [],
                "viewNum": 0,
                "cmtNum": 4,
                "shareNum": 0,
                "likeNum": 2,
                "imgs": [
                    {
                        "url": "http://111.231.87.118:3001/uploads/1538707477496-Screen Shot 2018-07-18 at 2.57.20 PM.png"
                    }
                ]
            },
            {
                "_id": "5bb431664757534285e8ffc1",
                "__v": 0,
                "content": "说来就来，666",
                "category": "缘分天空",
                "type": 0,
                "creator": {
                    "_id": "5bb1fab6e4ce4e194f53edbc",
                    "avatar": "http://www.hzic.edu.cn/UploadFiles/image/201809/2018090443662013.jpg",
                    "nickname": "财大小逗比"
                },
                "isAuth": true,
                "createTime": 1538535782,
                "isTop": true,
                "scores": [],
                "tags": [],
                "viewNum": 0,
                "cmtNum": 0,
                "shareNum": 0,
                "likeNum": 1,
                "imgs": []
            }
        ],
        "total": 2,
        "hasMore": false
    }
}
```

## 7. 浏览课程
* 地址

/api/viewPost 

* 入参
```
{
	"postId": "5bcb5f9c1036373333475c57"
}
```

* 出参

```
{
    "code": 0
}
```


## 8. 举报
* 地址

/api/saveReport 

* 入参
```
{
	"type": "user", // 用户user，评论msg，帖子post，二手secondhand，跑腿errand
	"targetId": "5bcb5f9c1036373333475c57", // userId, msgId, postId等
    "creator": {},
    "msg": "该帖子涉黄", // 举报者的话
    "reportId": "5bcb5f9c1036373333475c57" // 举报者ID
}
```

* 出参

```
{
    "code": 0
}
```

## 9. 给评论点赞
* 地址

/api/likePostMsg 

* 入参
```
{
	"_id": "5bcdfbab8ea4f57ff7b8d1d3" // 评论的_id
}
```

* 出参

```
{
    "code": 0
}
```

## 10. 删除评论点
* 地址

/api/removePostMsgs 

* 入参
```
{
	"postMsgIds": ["5bcdfbab8ea4f57ff7b8d1d3"] // 评论的_id
}
```

* 出参

```
{
    "code": 0
}
```

## 11. 获取banner
* 地址

/api/getBanners 

* 入参
```
{}
```

* 出参

```
{
    "code": 0,
    "data": [
        {
            "_id": "5bd4a3cc8dfd031633fbd136",
            "__v": 0,
            "title": "不错的哈",
            "img": "https://gd3.alicdn.com/imgextra/i2/1642714647/TB2Rx7wm8DH8KJjSspnXXbNAVXa_!!1642714647.jpg",
            "url": "http://111.231.87.118:3010/#/detail/5bd44e96e5597055b5a66a79",
            "createTime": 1540662220
        },
        {
            "_id": "5bd4a3f58dfd031633fbd138",
            "__v": 0,
            "title": "再来一波",
            "img": "https://gd3.alicdn.com/imgextra/i2/1642714647/TB2Rx7wm8DH8KJjSspnXXbNAVXa_!!1642714647.jpg",
            "url": "http://111.231.87.118:3010/#/detail/5bd48123f1d5bc651de87922",
            "createTime": 1540662261
        }
    ]
}
```

## 12. 发送消息
* 地址

/api/sendMessage 

* 入参
```
{
    "receiver": {"_id": '5bd4a3cc8dfd031633fbd136', "avatar": "url", nickname: "用户2"},
    "msg": "你好",
}
```

* 出参

```
{
    "code": 0
}
```

## 13. 获取消息列表
* 地址

/api/getMessages 

* 入参
```
{
    receiverId: 'xxxxx' // 聊天对方用户的_id
}
```

* 出参

```
{
    "code": 0,
    "data": [
        {
            "_id": "5bd4a3cc8dfd031633fbd136",
            "__v": 0,
            "sender": {"_id": '5bd4a3cc8dfd031633fbd136', "avatar": "url", nickname: "用户1"},
            "senderId": "5bd4a3cc8dfd031633fbd136",
            "receiver": {"_id": '5bd4a3cc8dfd031633fbd136', "avatar": "url", nickname: "用户2"},
            "receiverId": "5bd4a3cc8dfd031633fbd136",
            "msg": "你好",
            "createTime": 1540662220
        },
        {
            "_id": "5bd4a3cc8dfd031633fbd136",
            "__v": 0,
            "sender": {"_id": '5bd4a3cc8dfd031633fbd136', "avatar": "url", nickname: "用户1"},
            "senderId": "5bd4a3cc8dfd031633fbd136",
            "receiver": {"_id": '5bd4a3cc8dfd031633fbd136', "avatar": "url", nickname: "用户2"},
            "receiverId": "5bd4a3cc8dfd031633fbd136",
            "msg": "你也好",
            "createTime": 1540662220
        },
    ]
}
```

## 14. 获取客服信息
* 地址

/api/getServiceUser 

* 入参
```
{}
```

* 出参

```
{
    "code": 0,
    "data": {
        "code": 0,
        "data": {
            "_id": "5be847df571441451df2aaac",
            "avatar": "http://pic.90sjimg.com/design/00/07/85/23/58d7d6acefea6.jpg",
            "nickname": "客服"
        }
    }
}
```