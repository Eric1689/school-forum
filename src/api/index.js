let HOST = ''
// HOST = 'http://localhost:3001/'
// HOST = 'http://111.231.87.118:3001/'
// HOST = 'http://lairenna.com.cn/'

// if (location.hostname === '111.231.87.118') {
//   HOST = 'http://111.231.87.118:3001/'
// }
// if (location.hostname === 'lairenna.com.cn') {
//   HOST = 'http://lairenna.com.cn/'
// }
export default {
  // 分享
  share: '/site/wx-jsconfig',
  // 登录模块
  getAllSchools: HOST + 'api/getAllSchools',

  // 上传
  upload: HOST + 'api/upload',

  // 用户相关
  validateCode: HOST + 'api/getValidateCode', // 获取注册时候短信验证码
  loginValidateCode: HOST + 'api/getLoginValidateCode', // 获取登陆和修改密码时候的短信验证码
  register: HOST + 'api/reg', // 注册
  updateUserInfo: HOST + 'api/updateUserInfo', // 完善用户信息
  login: HOST + 'api/log', // 登陆
  getUser: HOST + 'api/user',
  getCaptcha: HOST + 'api/getCaptcha', // 获取重置密码时候的验证码
  resetPasswd: HOST + 'api/resetPasswd', // 重置密码
  dailySignin: HOST + 'api/dailySignin', // 每日签到
  validateCaptcha: HOST + 'api/validateCaptcha', // 验证验证码并发送短信
  watchUser: HOST + 'api/watchUser', // status 0 关注；1 取关
  viewUserInfo: HOST + 'api/viewUserInfo', // 查看用户详情
  getMyWatch: HOST + 'api/getMyWatch', // 我的关注列表
  getMyFan: HOST + 'api/getMyFan', // 我的粉丝列表
  getServiceUser: HOST + 'api/getServiceUser', // 获取客服

  // 帖子相关
  createPost: HOST + 'api/createPost', // 新建帖子
  getPostList: HOST + 'api/getPosts', // 帖子列表
  getActivities: HOST + 'api/getActivities', // 获取活动和投票列表
  joinActivity: HOST + 'api/joinActivity', // 参加投票或者活动
  cmtPost: HOST + 'api/cmtPost', // 评论帖子
  sharePost: HOST + 'api/sharePost', // 评论帖子
  getPost: HOST + 'api/getPost', // 获取帖子详情
  getMorePostMsgs: HOST + 'api/getMorePostMsgs', // 获取更多帖子评论
  // getAllPostLikes: HOST + 'api/getAllPostLikes', // 获取帖子的所有赞
  likePost: HOST + 'api/likePost', // 点赞帖子
  unWatchPost: HOST + 'api/unWatchPost', // 不再看某个帖子
  getRemovePost: HOST + 'api/removePost', // 软删除某个帖子
  removePosts: HOST + 'api/removePosts', // 删除某个帖子，直接批量，所有用数组
  getMyLikePosts: HOST + 'api/getMyLikePosts', // 我点赞的列表
  getMyWatchPost: HOST + 'api/getMyWatchPost', // 关注列表
  searchPosts: HOST + 'api/searchPosts', // 搜索帖子
  viewPost: HOST + 'api/viewPost', // 搜索帖子
  likePostMsg: HOST + 'api/likePostMsg', // 给评论点赞
  removePostMsgs: HOST + 'api/removePostMsgs', // 删除评论
  getBest10List: HOST + 'api/getBest10List', // 获取best10帖子

  // 跑腿相关
  createPaotui: HOST + 'api/createPaotui', // 新建跑腿
  obtainPaotui: HOST + 'api/obtainPaotui', // 抢单
  getPaotuiList: HOST + 'api/getPaotuiList', // 获取跑腿单列表（可以抢的订单）
  getMyObtainPaotuiList: HOST + 'api/getMyObtainPaotuiList', // 获取我抢到跑腿单
  getMyPaotuiList: HOST + 'api/getMyPaotuiList', // 获取我发布的跑腿单
  cancelPaotui: HOST + 'api/cancelPaotui', // 取消我发布的跑腿单
  cancelObtainPaotui: HOST + 'api/cancelObtainPaotui', // 取消我抢到的跑腿单
  finishPaotui: HOST + 'api/finishPaotui', // 设置完成我的跑腿单
  getMyObtainMoney: HOST + 'api/getMyObtainMoney', // 获取赚到的钱
  removeErrands: HOST + 'api/removeErrands', // 获取赚到的钱
  confirmPaotui: HOST + 'api/confirmPaotui', // 确认跑腿

  // 二手相关
  getSecondhands: HOST + 'api/getSecondhands', // 二手列表
  getMySecondhands: HOST + 'api/getMySecondhands', // 我发布的二手列表
  getMyObtainSecondhands: HOST + 'api/getMyObtainSecondhands', // 我获得的二手列表
  createSecondhand: HOST + 'api/createSecondhand', // 创建二手
  obtainSecondhand: HOST + 'api/obtainSecondhand', // 获得二手
  cmtSecondhand: HOST + 'api/cmtSecondhand', // 评论二手
  getSecondhand: HOST + 'api/getSecondhand', // 获取二手详情
  getMoreSecondhandMsgs: HOST + 'api/getMoreSecondhandMsgs', // 获取更多二手评论
  likeSecondhand: HOST + 'api/likeSecondhand', // 点赞二手
  searchSecondhands: HOST + 'api/searchSecondhands', // 搜索二手
  viewSecondhand: HOST + 'api/viewSecondhand', // 浏览二手

  // 课程相关
  // getCourseList: HOST + 'api/getCourses',
  getCourseList: HOST + 'api/getPosts',
  publishCourse: HOST + 'api/addCourse',
  cmtCourse: HOST + 'api/cmtCourse',
  getCourse: HOST + 'api/getCourse',

  // 通知相关
  getNotices: HOST + 'api/getNotices', // 获取消息列表数据，type=like点赞，cmt评论，system系统，other我的消息除了点赞和评论
  removeNotices: HOST + 'api/removeNotices',
  getNoticeNum: HOST + 'api/removeNotices', // 获取未读评论、点赞的通知数据
  readNotice: HOST + 'api/readNotice', // 读通知

  // 消息相关
  sendMessage: HOST + 'api/sendMessage',
  getMessages: HOST + 'api/getMessages',

  // 积分商城
  getGoods: HOST + 'api/getGoods', // 获取积分商品列表
  buyGood: HOST + 'api/buyGood', // 用积分换商品
  getAllBuyGoods: HOST + 'api/getAllBuyGoods', // 获取兑换的积分列表
  getPoints: HOST + 'api/getPoints', // 积分收支历史
  getLeftPoint: HOST + 'api/getLeftPoint', // 获取可用积分
  useLeftPoint: HOST + 'api/useLeftPoint', // 使用积分抽奖

  // 收藏相关
  saveCollect: HOST + 'api/saveCollect', // 添加到我的收藏
  getCollects: HOST + 'api/getCollects', // 获取收藏
  removeCollects: HOST + 'api/removeCollects', // 批量删除

  // 举报相关
  saveReport: HOST + 'api/saveReport', // 举报

  // 充值记录
  getTransfers: HOST + 'api/getTransfers', // 充值记录

  // 支付相关
  getPayParams: HOST + 'api/getPayParams', // 获取支付参数
  getPayResult: HOST + 'api/getPayResult', // 获取支付结果
  getChargeUser: HOST + 'api/chargeUser' // 充值
};
