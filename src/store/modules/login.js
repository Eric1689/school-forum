import axios from '../../js/axios'
import Api from '../../api'
const state = {
  login: {},
  register: {},
  validateCode: {},
  loginValidateCode: {},
  captcha: {},
  validateCaptcha: {},
  resetPasswd: {},
  userInfo: {}
}
const actions = {
  async getResetPasswd ({ commit }, params) {
    const res = await axios.post(Api.resetPasswd, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setResetPasswd', data) : msg
  },
  async getValidateCaptcha ({ commit }, params) {
    const res = await axios.post(Api.validateCaptcha, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setValidateCaptcha', data) : msg
  },
  async getCaptcha ({ commit }, params) {
    const res = await axios.post(Api.getCaptcha, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCaptcha', data) : msg
  },
  async getLoginValidateCode ({ commit }, params) {
    const res = await axios.post(Api.loginValidateCode, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setLoginValidateCode', data) : msg
  },
  async getValidateCode ({ commit }, params) {
    const res = await axios.post(Api.validateCode, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setValidateCode', data) : msg
  },
  async getLogin ({ commit }, params) {
    const res = await axios.post(Api.login, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setLogin', data) : msg
  },
  async getRegister ({ commit }, params) {
    const res = await axios.post(Api.register, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setRegister', data) : msg
  },
  async getViewUserInfo ({ commit }, params) {
    const res = await axios.post(Api.viewUserInfo, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setUserInfo', data) : msg
  }
}
const mutations = {
  setResetPasswd (state, data) {
    state.resetPasswd = data
  },
  setValidateCaptcha (state, data) {
    state.validateCaptcha = data
  },
  setCaptcha (state, data) {
    state.captcha = data
  },
  setLoginValidateCode (state, data) {
    state.loginValidateCode = data
  },
  setValidateCode (state, data) {
    state.validateCode = data
  },
  setLogin (state, data) {
    state.login = data
  },
  setRegister (state, data) {
    state.register = data
  },
  setUserInfo (state, data) {
    state.userInfo = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
