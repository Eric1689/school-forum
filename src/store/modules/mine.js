import axios from '../../js/axios'
import Api from '../../api'
const state = {
  dailySignin: {},
  watchList: [],
  watchObj: {},
  myFanList: [],
  myLikePosts: [],
  myLikePostObj: {},
  intergralList: {},
  buyIntergral: {},
  walletDetail: {},
  myCollects: {},
  lotteryData: {},
  lotteryNum: {},
  sendMessage: {},
  messages: {},
  serviceUser: {}
}
const actions = {
  async getServiceUser ({ commit }, params) {
    const res = await axios.post(Api.getServiceUser, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setServiceUser', data) : msg
  },
  async getMessages ({ commit }, params) {
    const res = await axios.post(Api.getMessages, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMessages', data) : msg
  },
  async getSendMessage ({ commit }, params) {
    const res = await axios.post(Api.sendMessage, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setSendMessage', data) : msg
  },
  async lottery ({ commit }, params) {
    const res = await axios.post(Api.useLeftPoint, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setLotteryData', data) : msg
  },
  async initlotteryNum ({ commit }, params) {
    const res = await axios.post(Api.getLeftPoint, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setLotteryNum', data) : msg
  },
  async getBuyIntergral ({ commit }, params) {
    const res = await axios.post(Api.buyGood, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setBuyIntergral', data) : msg
  },
  async getIntergralList ({ commit }, params) {
    const res = await axios.post(Api.getGoods, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setIntergralList', data) : msg
  },
  async getMyLikePosts ({ commit }, params) {
    const res = await axios.post(Api.getMyLikePosts, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyLikePosts', data) : msg
  },
  async getMyFan ({ commit }, params) {
    const res = await axios.post(Api.getMyFan, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyFan', data) : msg
  },
  async getMyWatch ({ commit }, params) {
    const res = await axios.post(Api.getMyWatch, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyWatch', data) : msg
  },
  async getDailySignin ({ commit }, params) {
    const res = await axios.post(Api.dailySignin, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setDailySignin', data) : msg
  },
  async getTransfers ({ commit }, params) {
    const res = await axios.post(Api.getTransfers, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setWalletDetail', data) : msg
  },
  async getCollects ({ commit }, params) {
    const res = await axios.post(Api.getCollects, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCollects', data) : msg
  }
}
const mutations = {
  setServiceUser (state, data) {
    state.serviceUser = data
  },
  setMessages (state, data) {
    state.messages = data
  },
  setSendMessage (state, data) {
    state.sendMessage = data
  },
  setLotteryData (state, data) {
    state.lotteryData = data
  },
  setLotteryNum (state, data) {
    state.lotteryNum = data
  },
  setBuyIntergral (state, data) {
    state.buyIntergral = data
  },
  setIntergralList (state, data) {
    state.intergralList = data
  },
  setMyLikePosts (state, data) {
    state.myLikePosts = data
    state.myLikePostObj = data.reduce((t, c) => {
      t[c.postId] = true
      return t
    }, {})
  },
  setMyFan (state, data) {
    state.myFanList = data
  },
  setMyWatch (state, data) {
    state.watchList = data
    state.watchObj = data.reduce((t, c) => {
      t[c.user2Id] = true
      return t
    }, {})
  },
  setDailySignin (state, data) {
    state.dailySignin = data
  },
  setWalletDetail (state, data) {
    state.walletDetail = data
  },
  setCollects (state, data) {
    state.myCollects = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
