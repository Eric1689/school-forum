import axios from '../../js/axios'
import Api from '../../api'
const state = {
  secondhandsList: {},
  paotuiList: {},
  noticeList: {},
  postList: {},
  best10List: {},
  courseList: {},
  myPaotuiList: {},
  activities: {},
  mySecondhands: {},
  obtainPaotui: {},
  confirmPaotui: {},
  myObtainPaotuiList: {},
  cancelPaotui: {},
  cancelObtainPaotui: {},
  removeErrand: {},
  myObtainMoney: 0,
  courseDetail: {},
  watchUserPostList: [],
  searchPostList: [],
  messages: [],
  fenxiangVal: { show: false, item: {} },
  pointList: [
    { title: '教得好', val: 0 },
    { title: '挂科少', val: 0 },
    { title: '给分高', val: 0 },
    { title: '点名少', val: 0 },
    { title: '作业少', val: 0 }
  ],
  tagsList: [
    { id: '1', name: '气质好' },
    { id: '2', name: '激情四射' },
    { id: '3', name: '治学严谨' },
    { id: '4', name: '颜值高' },
    { id: '5', name: '博学多才' },
    { id: '6', name: '平易近人' },
    { id: '7', name: '妙语解颐' },
    { id: '8', name: '严厉严肃' }
  ],
  tagsObject: {
    1: '气质好',
    2: '激情四射',
    3: '治学严谨',
    4: '颜值高',
    5: '博学多才',
    6: '平易近人',
    7: '妙语解颐',
    8: '严厉严肃'
  },
  readnotice: {}
}
const actions = {
  async getReadnotice ({ commit }, params) {
    const res = await axios.post(Api.readNotice, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setReadnotice', data) : msg
  },
  async getMessages ({ commit }, params) {
    const res = await axios.post(Api.getMessages, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMessages', data) : msg
  },
  async getCourse ({ commit }, params) {
    const res = await axios.post(Api.getCourse, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCourse', data) : msg
  },
  async getMyObtainMoney ({ commit }, params) {
    const res = await axios.post(Api.getMyObtainMoney, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyObtainMoney', data) : msg
  },
  async getCancelObtainPaotui ({ commit }, params) {
    const res = await axios.post(Api.cancelObtainPaotui, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCancelObtainPaotui', data) : msg
  },
  async getCancelPaotui ({ commit }, params) {
    const res = await axios.post(Api.cancelPaotui, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCancelPaotui', data) : msg
  },
  async getRemoveErrand ({ commit }, params) {
    const res = await axios.post(Api.removeErrands, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setRemoveErrand', data) : msg
  },
  async getMyObtainPaotuiList ({ commit }, params) {
    const res = await axios.post(Api.getMyObtainPaotuiList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyObtainPaotuiList', data) : msg
  },
  async getObtainPaotui ({ commit }, params) {
    const res = await axios.post(Api.obtainPaotui, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setObtainPaotui', data) : msg
  },
  async getMySecondhands ({ commit }, params) {
    const res = await axios.post(Api.getMySecondhands, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMySecondhands', data) : msg
  },
  async getActivities ({ commit }, params) {
    const res = await axios.post(Api.getActivities, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setActivities', data) : msg
  },
  async getMyPaotuiList ({ commit }, params) {
    const res = await axios.post(Api.getMyPaotuiList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setMyPaotuiList', data) : msg
  },
  async getSecondhands ({ commit }, params) {
    const res = await axios.post(Api.getSecondhands, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setSecondhands', data) : msg
  },
  async getPaotuiList ({ commit }, params) {
    const res = await axios.post(Api.getPaotuiList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setPaotuiList', data) : msg
  },
  async getNotices ({ commit }, params) {
    const res = await axios.post(Api.getNotices, params)
    const { code, data, msg } = res
    return code === 0 ? commit('saveNotices', data) : msg
  },
  async getPostList ({ commit }, params) {
    const res = await axios.post(Api.getPostList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setPostList', data) : msg
  },
  async getCourseList ({ commit }, params) {
    const res = await axios.post(Api.getCourseList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCourseList', data) : msg
  },
  async getWatchUserPostList ({ commit }, params) {
    const res = await axios.post(Api.getMyWatchPost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setWatchUserPostList', data) : msg
  },
  async getSearchPostList ({ commit }, params) {
    const res = await axios.post(Api.searchPosts, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setSearchPostList', data) : msg
  },
  async getBest10List ({ commit }, params) {
    const res = await axios.post(Api.getBest10List, params)
    const { code, data, msg } = res
    return code === 0 ? commit('saveBest10List', data) : msg
  },
  async getRemovePost ({ commit }, params) {
    const res = await axios.post(Api.getRemovePost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('saveBest10List', data) : msg
  },
  async getConfirmPaotui ({ commit }, params) {
    const res = await axios.post(Api.confirmPaotui, params)
    const { code, data, msg } = res
    return code === 0 ? commit('saveConfirmPaotui', data) : msg
  }
}
const mutations = {
  setReadnotice (state, data) {
    state.readnotice = data
  },
  setCourse (state, data) {
    state.courseDetail = data
  },
  setFenxiangVal (state, data) {
    state.fenxiangVal = data
  },
  setMessages (state, data) {
    state.messages = data
  },
  setMyObtainMoney (state, data) {
    state.myObtainMoney = data
  },
  getCancelObtainPaotui (state, data) {
    state.cancelObtainPaotui = data
  },
  getRemoveErrand (state, data) {
    state.removeErrand = data
  },
  setCancelPaotui (state, data) {
    state.cancelPaotui = data
  },
  setObtainPaotui (state, data) {
    state.obtainPaotui = data
  },
  setMySecondhands (state, data) {
    state.mySecondhands = data
  },
  setActivities (state, data) {
    state.activities = data
  },
  setMyPaotuiList (state, data) {
    state.myPaotuiList = data
  },
  setMyObtainPaotuiList (state, data) {
    state.myObtainPaotuiList = data
  },
  setCourseList (state, data) {
    state.courseList = data
  },
  setSecondhands (state, data) {
    state.secondhandsList = data
  },
  setPaotuiList  (state, data) {
    state.paotuiList = data
  },
  saveNotices  (state, data) {
    state.noticeList = data
  },
  setPostList (state, data) {
    state.postList = data
  },
  setWatchUserPostList (state, data) {
    state.watchUserPostList = data
  },
  setSearchPostList (state, data) {
    state.searchPostList = data
  },
  saveBest10List (state, data) {
    state.best10List = data
  },
  saveConfirmPaotui (state, data) {
    state.confirmPaotui = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
