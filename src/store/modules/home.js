import axios from '../../js/axios'
import Api from '../../api'
const state = {
  postList: {},
  upload: {},
  updateUserInfo: {},
  watchUser: {},
  unWatchPost: {},
  likePost: {},
  likePostMsg: {},
  WxShare: {},
  topPostList: {},
  schoolArr: {},
  rechangeData: {},
  reportRes: {},
  saveCollect: {},
  chargeUserData: {}
}
const actions = {
  async getSaveCollect ({ commit }, params) {
    const res = await axios.post(Api.saveCollect, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setSaveCollect', data) : msg
  },
  async getWxShare ({ commit }, params) {
    const res = await axios.post(Api.share, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setWxShare', data) : msg
  },
  async getRechargeData ({ commit }, params) {
    const res = await axios.post(Api.getPayParams, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setRechargeData', data) : msg
  },
  async saveRemovePostMsgs ({ commit }, params) {
    const res = await axios.post(Api.removePostMsgs, params)
    return res;
  },
  async getLikePost ({ commit }, params) {
    const res = await axios.post(Api.likePost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setLikePost', data) : msg
  },
  async getlikePostMsg ({ commit }, params) {
    const res = await axios.post(Api.likePostMsg, params)
    // const { code, data, msg } = res
    // return code === 0 ? commit('setLikePostMsg', res) : msg
    return res
  },
  async getUnWatchPost ({ commit }, params) {
    const res = await axios.post(Api.unWatchPost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setUnWatchPost', data) : msg
  },
  async getWatchUser ({ commit }, params) {
    const res = await axios.post(Api.watchUser, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setWatchUser', data) : msg
  },
  async getUpdateUserInfo ({ commit }, params) {
    const res = await axios.post(Api.updateUserInfo, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setUpdateUserInfo', data) : msg
  },
  async getPostList ({ commit }, params) {
    const res = await axios.post(Api.getPostList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setPostList', data) : msg
  },
  async getTopPostList ({ commit }, params) {
    const res = await axios.post(Api.getPostList, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setTopPostList', data) : msg
  },
  async getUpload ({ commit }, params) {
    const res = await axios.post(Api.upload, params)
    const { result, data, msg } = res
    return result === 0 ? commit('setUpload', data) : msg
  },
  async getAllSchools ({ commit }, params) {
    const res = await axios.post(Api.getAllSchools, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setAllSchools', data) : msg
  },
  async saveReport ({ commit }, params) {
    const res = await axios.post(Api.saveReport, params)
    const { code, data, msg } = res
    return code === 0 ? commit('saveReportRes', data) : msg
  },
  async getChargeUserData ({ commit }, params) {
    const res = await axios.post(Api.getChargeUser, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setChargeUserData', data) : msg
  }
}
const mutations = {
  setSaveCollect (state, data) {
    state.saveCollect = data;
  },
  setWxShare (state, data) {
    state.WxShare = data;
  },
  setRechargeData (state, data) {
    state.rechargeData = data;
  },
  setLikePost (state, data) {
    state.likePost = data
  },
  setLikePostMsg (state, data) {
    state.likePostMsg = data
  },
  setUnWatchPost (state, data) {
    state.unWatchPost = data
  },
  setWatchUser (state, data) {
    state.watchUser = data
  },
  setUpdateUserInfo (state, data) {
    state.updateUserInfo = data
  },
  setUpload (state, data) {
    state.upload = data
  },
  setPostList (state, data) {
    state.postList = data
  },
  setTopPostList (state, data) {
    state.topPostList = data
  },
  setAllSchools (state, data) {
    state.schoolArr = data
  },
  saveReportRes (state, data) {
    state.reportRes = data
  },
  setChargeUserData (state, data) {
    state.chargeUserData = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
