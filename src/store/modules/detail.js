import axios from '../../js/axios'
import Api from '../../api'
const state = {
  detail: { cmt: { cmts: [] }, likes: [], _id: '', creator: { _id: '' } },
  cmtPost: {}
}
const actions = {
  async getDetail ({ commit }, params) {
    const res = await axios.post(Api.getPost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setDetail', data) : msg
  },
  async getCmtPost ({ commit }, params) {
    const res = await axios.post(Api.cmtPost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCmtPost', data) : msg
  }
}
const mutations = {
  setDetail (state, data) {
    state.detail = data
  },
  setCmtPost (state, data) {
    state.cmtPost = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
