import axios from '../../js/axios'
import Api from '../../api'
const state = {
  schools: [],
  schoolOptions: [],
  login: {},
  postList: {},
  user: {},
  school: {},
  errandList: [],
  courseList: [],
  courseData: {},
  secondList: [],
  paotuiListData: {},
  myPaotuiListData: {}
}
const actions = {
  async getScools ({ commit }, params) {
    const res = await axios.post(Api.getAllSchools, params)
    const { code, data, msg } = res
    if (code === 0) {
      commit('saveSchools', data)
    } else {
      return msg
    }
  },
  async getLogin ({ commit }, params) {
    const res = await axios.post(Api.login, params)
    const { code, data, msg } = res
    if (code === 0) {
      const { user } = data;
      commit('saveUser', user)
      return true
    } else {
      return msg
    }
  },
  async getDataList ({ commit }, params) {
    const data = await axios.post(Api.getPostList, params)
    const { code, message } = data
    if (code === 0) {
      commit('savePostList', data)
    } else {
      return message
    }
  },
  async publishPost ({ commit }, params) {
    const data = await axios.post(Api.publishPost, params)
    const { code, message } = data
    if (code === 0) {
      // commit('dataListMock', data)
      return true
    } else {
      return message
    }
  },
  async getErrandList ({ commit }, params) {
    const data = await axios.post(Api.getErrandList, params)
    const { code, message } = data
    if (code === 0) {
      commit('saveErrandList', data)
    } else {
      return message
    }
  },
  async publishErrand ({ commit }, params) {
    const data = await axios.post(Api.publishErrand, params)
    const { code, message } = data
    if (code === 0) {
      // commit('dataListMock', data)
      return true
    } else {
      return message
    }
  },
  async getPaotuiList ({ commit }, params) {
    const data = await axios.post(Api.getPaotuiList, params)
    const { code, message } = data
    if (code === 0) {
      commit('savePaotuiList', data)
    } else {
      return message
    }
  },

  async getMyPaotuiList ({ commit }, params) {
    const data = await axios.post(Api.getMyPaotuiList, params)
    const { code, message } = data
    if (code === 0) {
      commit('saveMyPaotuiList', data)
    } else {
      return message
    }
  },

  async getCourseList ({ commit }, params) {
    const data = await axios.post(Api.getCourseList, params)
    const { code, message } = data
    if (code === 0) {
      commit('saveCourseList', data)
    } else {
      return message
    }
  },
  async publishCourse ({ commit }, params) {
    const data = await axios.post(Api.publishCourse, params)
    const { code, message } = data
    if (code === 0) {
      // commit('dataListMock', data)
      return true
    } else {
      return message
    }
  },
  async getCourse ({ commit }, params) {
    const data = await axios.post(Api.getCourse, params)
    const { code, message } = data
    if (code === 0) {
      commit('saveCourseData', data)
    } else {
      return message
    }
  },
  async getSecondList ({ commit }, params) {
    const data = await axios.post(Api.getSecondhands, params)
    const { code, message } = data
    if (code === 0) {
      commit('saveSecondList', data)
    } else {
      return message
    }
  },
  async publishSecond ({ commit }, params) {
    const data = await axios.post(Api.publishSecond, params)
    const { code, message } = data
    if (code === 0) {
      // commit('dataListMock', data)
      return true
    } else {
      return message
    }
  }
}
const mutations = {
  saveSchools (state, data) {
    state.schools = data
    state.schoolOptions = data.map(({_id: key, name: value}) => ({key, value}))
  },
  savePostList (state, data) {
    state.postList = data
  },
  login (state, data) {
    state.login = data
  },
  saveUser (state, data) {
    state.user = data
    const school = state.schools.filter(s => s._id === data.schoolId)[0]
    school.deliveryPlaces = school.deliveryPlaces.split(',')
    state.school = school
  },
  saveErrandList (state, data) {
    state.errandList = data
  },
  savePaotuiList (state, data) {
    state.paotuiListData = data
  },
  saveMyPaotuiList  (state, data) {
    state.myPaotuiListData = data
  },
  saveCourseList (state, data) {
    state.courseList = data
  },
  saveCourseData (state, data) {
    state.courseData = data
  },
  saveSecondList (state, data) {
    state.secondList = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
