import axios from '../../js/axios'
import Api from '../../api'
const state = {
  createPost: {},
  createPaotui: {},
  addCourse: {},
  cmtCourse: {},
  createSecondhand: {}
}
const actions = {
  async getCreatePost ({ commit }, params) {
    const res = await axios.post(Api.createPost, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCreatePost', data) : msg
  },
  async getCreateSecondhand ({ commit }, params) {
    const res = await axios.post(Api.createSecondhand, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCreateSecondhand', data) : msg
  },
  async getCreatePaotui ({ commit }, params) {
    const res = await axios.post(Api.createPaotui, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCreatePaotui', data) : msg
  },
  async getAddCourse ({ commit }, params) {
    const res = await axios.post(Api.publishCourse, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setAddCourse', data) : msg
  },
  async getCmtCourse ({ commit }, params) {
    const res = await axios.post(Api.cmtCourse, params)
    const { code, data, msg } = res
    return code === 0 ? commit('setCmtCourse', data) : msg
  }
}
const mutations = {
  setCreatePost (state, data) {
    state.createPost = data
  },
  setCreatePaotui (state, data) {
    state.createPaotui = data
  },
  setCreateSecondhand (state, data) {
    state.createSecondhand = data
  },
  setAddCourse (state, data) {
    state.addCourse = data
  },
  setCmtCourse (state, data) {
    state.cmtCourse = data
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
