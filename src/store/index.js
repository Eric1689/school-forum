import Vue from 'vue';
import Vuex from 'vuex';
import test from './modules/test';
import home from './modules/home';
import login from './modules/login';
import publish from './modules/publish';
import list from './modules/list';
import detail from './modules/detail';
import mine from './modules/mine';
Vue.use(Vuex);
export default new Vuex.Store({
  modules: { test, home, login, publish, list, detail, mine }
});
