
import moment from 'moment';
export const getUnixTime = (date) => {
  return date ? moment(date).unix() : 0
}
export const getUuid = () => {
  let d = new Date().getTime();
  let uuId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    let r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuId
}
// 支付宝浏览器支付
export const payReady = callback => {
  // 如果jsbridge已经注入则直接调用
  if (window.AlipayJSBridge) {
    callback && callback();
  } else {
    // 如果没有注入则监听注入的事件
    document.addEventListener('AlipayJSBridgeReady', callback, false);
  }
}
export const alipayApp = (orderStr, callback) => {
  payReady(() => {
    window.AlipayJSBridge.call('tradePay', {
      orderStr: orderStr
    }, result => {
      callback(result)
    });
  });
}
