import axios from 'axios'
import router from '../router'
// import Qs from 'qs'
const Axios = axios.create({
  baseURL: '/',
  timeout: 10000,
  responseType: 'json',
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
    // 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
  }
  // transformRequest: [data => Qs.stringify(data)]
})
Axios.interceptors.request.use(config => {
  // if (config.method === 'post' || config.method === 'put' || config.method === 'delete') {
  //   config.data = Qs.stringify(config.data)
  // }
  const loginInfo = localStorage.getItem('loginInfo');
  if (loginInfo) {
    config.headers.token = JSON.parse(loginInfo).token
  }
  return config
}, error => {
  return Promise.reject(error)
})
Axios.interceptors.response.use(res => {
  return res.data
}, error => {
  switch (error.response.status) {
    case 401:
      router.push('/login')
      break
  }
  return Promise.reject(error.response.statusText)
})
export default Axios
