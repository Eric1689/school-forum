import Vue from 'vue';
import Router from 'vue-router';
const NotFind = () => import('@/components/NotFind');
const Index = () => import('@/views');
const Home = () => import('@/views/Home');
const Message = () => import('@/views/Message');
const Order = () => import('@/views/Order');
const mine = () => import('@/views/Mine');
const Agreement = () => import('@/views/agreement');
const SoftwareAgreement = () => import('@/views/agreement/Software');
const ForumAgreement = () => import('@/views/agreement/Forum');
const NeederAgreement = () => import('@/views/agreement/Needer');
const PlayFormAgreement = () => import('@/views/agreement/PlayForm');
const SecondHandAgreement = () => import('@/views/agreement/SecondHand');
const ServiceAgreement = () => import('@/views/agreement/Service');
const ForumIndex = () => import('@/views/forum/ForumIndex');
const ForumWatch = () => import('@/views/forum/ForumWatch');
const ForumSquare = () => import('@/views/forum/ForumSquare');
const Login = () => import('@/views/login/Login');
const Register = () => import('@/views/login/Register');
const PersonInfo = () => import('@/views/login/PersonInfo');
const PersonIndex = () => import('@/views/login/PersonIndex');
// const PersonHome = () => import('@/views/login/PersonHome'); // 用户主页（未做）
const SchoolInfo = () => import('@/views/login/SchoolInfo');
const SelectMajor = () => import('@/views/login/SelectMajor');
const PublishPost = () => import('@/views/publish/PublishPost');
const PublishSecond = () => import('@/views/publish/PublishSecond');
const Secondhand = () => import('@/views/second/Secondhand');
const SecondhandList = () => import('@/views/second/SecondhandList');
const ErrandIndex = () => import('@/views/errand/ErrandIndex');
const ForgetPassword = () => import('@/views/login/ForgetPassword');
const ValidCode = () => import('@/views/login/ValidCode');
const Wallet = () => import('@/views/wallet/Wallet');
const WalletDetail = () => import('@/views/wallet/WalletDetail');
const WalletCharge = () => import('@/views/wallet/WalletCharge');
const Settings = () => import('@/views/Set/Settings');
const SubmitPage = () => import('@/views/common/SubmitPage');
const Search = () => import('@/views/common/Search');
const Report = () => import('@/views/common/Report');
const ActivityAdd = () => import('@/views/activity/ActivityAdd');
const ActivityDetail = () => import('@/views/activity/ActivityDetail');
const VoteAdd = () => import('@/views/vote/VoteAdd');
const Fatedetail = () => import('@/views/square/Fatedetail');
const CourseList = () => import('@/views/square/Course');
const CourseDetail = () => import('@/views/course/CourseDetail');
const CourseDetailCmt = () => import('@/views/course/CourseDetailCmt');
const CourseAdd = () => import('@/views/course/CourseAdd');
const ItemDetail = () => import('@/views/common/ItemDetail');
const MyPost = () => import('@/views/my/MyPost');
const MyAttention = () => import('@/views/my/MyAttention');
const MyFans = () => import('@/views/my/MyFans');
const MyLikes = () => import('@/views/my/MyLikes');
const MyCollection = () => import('@/views/my/MyCollection');
const IntegralShop = () => import('@/views/integral');
const IntegralDetail = () => import('@/views/integral/integraldetail');
const IntegralRes = () => import('@/views/integral/res');
const RechargeRecords = () => import('@/views/wallet/RechargeRecords');
const Lottery = () => import('@/views/my/Lottery');
const ChatList = () => import('@/views/common/ChatList');
const About = () => import('@/views/set/About');

Vue.use(Router);
const router = new Router({
  routes: [
    {
      path: '',
      component: Index,
      redirect: '/home',
      children: [
        { name: 'home', path: '/home', component: Home, meta: { keepAlive: true, requireLogin: false } },
        { name: 'message', path: '/message', component: Message, meta: { requireLogin: true } },
        { name: 'order', path: '/order', component: Order },
        { name: 'mine', path: '/mine', component: mine }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/lottery',
      name: 'lottery',
      component: Lottery
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/schoolinfo',
      name: 'schoolinfo',
      component: SchoolInfo,
      meta: { keepAlive: true }
    },
    {
      path: '/courselist',
      name: 'courselist',
      component: CourseList,
      meta: { keepAlive: true, requireLogin: true }
    },
    {
      path: '/personinfo',
      name: 'personinfo',
      component: PersonInfo,
      meta: { keepAlive: true }
    },
    {
      path: '/personindex',
      name: 'personindex',
      component: PersonIndex,
      meta: { keepAlive: true }
    },
    // {
    //   path: '/personhome/:id',
    //   name: 'personhome',
    //   component: PersonHome
    // },
    {
      path: '/selectmajor',
      name: 'selectmajor',
      component: SelectMajor
    },
    {
      path: '/agreement',
      name: 'agreement',
      component: Agreement
    },
    {
      path: '/neederAgreement',
      name: 'neederAgreement',
      component: NeederAgreement
    },
    {
      path: '/softwareAgreement',
      name: 'softwareAgreement',
      component: SoftwareAgreement
    },
    {
      path: '/serviceAgreement',
      name: 'serviceAgreement',
      component: ServiceAgreement
    },
    {
      path: '/secondhandAgreement',
      name: 'secondhandAgreement',
      component: SecondHandAgreement
    },
    {
      path: '/playformAgreement',
      name: 'playformAgreement',
      component: PlayFormAgreement
    },
    {
      path: '/forumAgreement',
      name: 'forumAgreement',
      component: ForumAgreement
    },
    {
      path: '/forum',
      name: 'forum',
      redirect: 'square',
      component: ForumIndex,
      meta: { keepAlive: true },
      children: [
        { name: 'watch', path: 'watch', component: ForumWatch, meta: { keepAlive: true } },
        { name: 'square', path: 'square', component: ForumSquare, meta: { keepAlive: true } }
      ]
    },
    // {
    //   path: '/activity',
    //   name: 'activity',
    //   component: Activity
    // },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/chatlist/:id',
      name: 'chatlist',
      component: ChatList
    },
    {
      path: '/submitpage',
      name: 'submitpage',
      component: SubmitPage
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: ItemDetail
    },
    {
      path: '/activityadd',
      name: 'activityadd',
      component: ActivityAdd,
      meta: { requireLogin: true }
    },
    {
      path: '/activitydetail/:id',
      name: 'activitydetail',
      component: ActivityDetail
    },
    {
      path: '/voteadd',
      name: 'voteadd',
      component: VoteAdd,
      meta: { requireLogin: true }
    },
    {
      path: '/fatedetail/:id',
      name: 'fatedetail',
      component: Fatedetail
    },
    // {
    //   path: '/vote',
    //   name: 'votelist',
    //   component: VoteList
    // },
    {
      path: '/coursedetail/:id',
      name: 'coursedetail',
      component: CourseDetail
    },
    {
      path: '/coursedetailcmt/:id',
      name: 'coursedetailcmt',
      component: CourseDetailCmt
    },
    {
      path: '/courseadd',
      name: 'courseadd',
      component: CourseAdd,
      meta: { requireLogin: true }
    },
    {
      path: '/secondhand',
      name: 'secondhand',
      component: Secondhand
    },
    {
      path: '/report',
      name: 'report',
      component: Report,
      meta: { requireLogin: true }
    },
    {
      path: '/secondhandList/:id',
      name: 'secondhandList',
      component: SecondhandList,
      meta: { keepAlive: true }
    },
    {
      path: '/mypost',
      name: 'mypost',
      component: MyPost,
      meta: { keepAlive: true }
    },
    {
      path: '/mycollection',
      name: 'mycollection',
      component: MyCollection,
      meta: { keepAlive: true }
    },
    {
      path: '/integral',
      name: 'integral',
      component: IntegralShop
    },
    {
      path: '/myattention',
      name: 'myattention',
      component: MyAttention,
      meta: { keepAlive: true }
    },
    {
      path: '/errand/:type',
      name: 'errand',
      component: ErrandIndex
    },
    {
      path: '/publishpost',
      name: 'publishpost',
      component: PublishPost,
      meta: { requireLogin: true, toPersonInfo: true }
    },
    {
      path: '/publishsecond',
      name: 'publishsecond',
      component: PublishSecond,
      meta: { requireLogin: true, toPersonInfo: true }
    },
    {
      path: '/forgetpassword',
      name: 'forgetpassword',
      component: ForgetPassword
    },
    {
      path: '/validcode',
      name: 'validcode',
      component: ValidCode
    },
    {
      path: '/wallet',
      name: 'wallet',
      component: Wallet
    },
    {
      path: '/walletdetail',
      name: 'walletdetail',
      component: WalletDetail
    },
    {
      path: '/walletcharge',
      name: 'walletcharge',
      component: WalletCharge
    },
    {
      path: '/rechargerecords/:id',
      name: 'rechargerecords',
      component: RechargeRecords
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: { requireLogin: true }
    },
    {
      path: '/myfans',
      name: 'myfans',
      component: MyFans
    },
    {
      path: '/integraldetail',
      name: 'integraldetail',
      component: IntegralDetail
    },
    {
      path: '/integralres',
      name: 'integralres',
      component: IntegralRes
    },
    {
      path: '/mylikes',
      name: 'mylikes',
      component: MyLikes,
      meta: { keepAlive: true }
    },
    { path: '*', name: 'notFind', component: NotFind }
  ],
  mode: 'hash',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
});
router.beforeEach((to, from, next) => {
  to.meta.keepAlive = from.name === 'detail'
  if (to.meta.requireLogin) {
    const loginInfo = window.localStorage.getItem('loginInfo')
    if (loginInfo) {
      if (to.meta.toPersonInfo) {
        if (JSON.parse(loginInfo).user.nickname) {
          next()
        } else {
          next({
            path: '/personinfo'
          })
        }
      } else {
        next()
      }
    } else {
      next({
        path: '/login'
      })
    }
  } else {
    next()
  }
})

export default router;
