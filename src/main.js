import 'babel-polyfill'
import Vue from 'vue';
import Es6Promise from 'es6-promise'
import './assets/iconfont/iconfont.css';
import Mint from 'mint-ui';
import 'mint-ui/lib/style.css';
import App from './App';
// import 'es6-promise/auto';
import Vuex from 'vuex';
import router from './router';
import store from './store';
import moment from 'moment';
import 'lib-flexible/flexible'
import VueLazyLoad from 'vue-lazyload'
import VueScroller from 'vue-scroller'
import gallery from 'img-vuer'
import wx from 'weixin-js-sdk'


import {
  Toast,
  ToastPlugin,
  LoadingPlugin,
  ConfirmPlugin
} from 'vux'
import VueClipboard from 'vue-clipboard2'
require('es6-promise').polyfill()
Es6Promise.polyfill()
Vue.use(VueClipboard)
Vue.config.productionTip = false;
Vue.use(Vuex);
Vue.use(Mint);
Vue.use(Toast);
Vue.use(ToastPlugin);
Vue.use(LoadingPlugin);
Vue.use(VueScroller);
Vue.use(ConfirmPlugin);
Vue.use(gallery)
Vue.prototype.$wx = wx;
Vue.use(VueLazyLoad, {
  error: '../static/error.png',
  loading: '../static/loading.gif'
})
Vue.filter('datetimestrformat', (dateStr, pattern = 'YYYY-MM-DD HH:mm:ss') => dateStr ? moment(dateStr).format(pattern) : '')
Vue.filter('datetimeformat', (timestamp, pattern = 'YYYY-MM-DD HH:mm:ss') => timestamp ? moment(timestamp * 1000).format(pattern) : '')
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
