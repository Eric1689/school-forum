#

| 序号 | 表名 | 说明 |
| -- | -- |-- |
| 1 |  user | 用户 |
| 2 | school | 学校 |
| 3 | post	| 帖子 | 
| 4 | postMsg	| 帖子留言 | 
| 5 | secondhand	| 二手 |
| 6 | errand	| 跑腿 |
| 7 | postWatch	| 帖子关注 |
| 8 | postDelivery	| 帖子转发 |
| 9 | postCollect	| 帖子收藏 |
| 10 | user2User	| 用户关系（关注/屏蔽） |
| 11 | account	| 账号概况 |
| 12 | accountDetail	| 账号使用情况 |
| 13 | ad	| 广告 |
| 14 | ad	| 二手货点赞 |
| 15 | ad	| 二手货留言 |
| 16 | activity	| 社区活动 |
| 17 | vote	| 投票 |
| 18 | course	| 课程 |

## 1. user

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| tel | string	| 手机号 |
| passwd | string	| 密码 |  
| nickname | string	| 昵称 |
| avatar | url	| 头像 |
| mail | url	| edu邮箱 |
| schoolId | string	| 所属学校 |
| collegeId | string	| 学院 |
| grade | string	| 年级 |
| sId(studentId) | string	| 学号 |
| idImgs | array	| 证件图片 |
| realName | string	| 真实姓名 |
| isAuth | bool	| 是否实名认证 |


## 2. school

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| name | string	| 大学名称 |
| shortName | string	| 论坛名称 |  
| logo | string	| 图片（可能是学校logo） |
| bssName | string	| 论坛名称 |  
| colleges | array	| 二级学院 |  
| deliveryPlaces | array	| 快递点 |
| bbsCategories | string	| 论坛分类 |

## 3. post

> activity和vote要能够在贴吧出现

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| title | string	| 标题（activity和vote） |
| content | string	| 内容 |
| votes | Array	| 投票的选项，文字，图片，票数 |
| imgs | Array of url	| 附件图片 |  
| category | string	| 所属分类 | 
| secCategory | string	| 所属分类 |  
| createTime | int	| 创建时间 |  
| deadline | int	| 截止时间 | 
| creator | Object	| 创建者Id, avatar, nickName |  

## 4. postMsg

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| content | string	| 内容（对于投票就是选择的项目） |
| postId | string	| 帖子id |
| postId | string	| 留言用户 |
| createTime | int	| 创建时间 |
| createTime | int	| 是否删除 | 

## 5. secondhand

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| title | string	| 标题（为搜索） |
| content | string	| 内容 |
| imgs | Array of url	| 附件图片 |  
| category | string	| 所属分类 | 
| secCategory | string	| 所属分类 |   
| createTime | int	| 创建时间 |  
| deadline | int	| 截止时间 |
| createTime | int	| 状态 |
| creator | Object	| 创建者Id, avatar, nickName |  
| buyer | Object	| 购买者Id, avatar, nickName | 

## 6. errand

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| fromPlace | string	| 取自 |
| toPlace | string	| 送达 |
| startTime | int	| 开始时间 |   
| endTime | int	| 结束时间 | 
| tel | string	| 联系方式 | 
| title | string	| 物品描述 |
| content | string	| 详细说明 |
| imgs | Array of url	| 附件图片 |
| command | int	| 服务要求，男女 |  
| price | int	| 价格 | 
| type | int	| 类型，跑腿或者帮帮忙 | 
| creator | Object	| 发布者（头像，昵称，手机号） |  
| getter | Object	| 抢到者（头像，昵称，手机号） | 
| status | int	| 状态（待付款，待接单，待开始-，待完成，待评价（对跑手的评价），被取消，已完成） | 


## 7 postWatch

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| postId | string	| 帖子id |
| userId | string	| 关注的用户id |

## 8. postDelivery

## 9. postCollect

## 10. user2User

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| userId | string	| 用户id |
| user2Id | string	| 相对着id |
| type | string	| 0为关注 1为屏蔽 |

## 11. account

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| userId | string	| 用户（昵称，头像） |
| money | number	| 充值账户 |
| virtual | number	| 收益账户 |


## 12. accountDetail

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| userId | string	| 用户id |
| createTime | string	| 相对着id |
| type | string	| 0为钱 1为虚拟 |
| isUse | string	| 0为充值或赚到 1为使用（相对） |
| desc | string	| 说明- |


## 13. ad

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| img | url	| 广告图片地址 |
| adUrl | url	| 广告对应页面的地址 |
| des | string	| 广告说明 |

## 17. vote

| 字段 | 类型 | 说明 |
| -- |-- |-- |
| des | string	| 标题 |
| des | string	| 说明 |
| des | string	| 选项 |
| des | string	| 发起人 |
| des | string	| 截止时间 |

## 18. course
| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| courseName | string	| 所教课程 |
| teacher | string	| 老师姓名 |
| tags | Object	| 标签，使用对象可以防止重复 |
| msgs | Object	| 评论的对象 |
| score | Object	| 各类打分的分数 |

<!-- ## 19. courseScore
| 字段 | 类型 | 说明 |
| -- |-- |-- |
| id | string | 唯一id |
| courseId | url	| 课程ID |
| msg | string	| 评论 | -->